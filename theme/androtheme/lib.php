<?php

defined('MOODLE_INTERNAL') || die();

function theme_androtheme_process_css($css, $theme) {
    
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_androtheme_set_logo($css, $logo);

    //Set custom CSS
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_androtheme_set_customcss($css, $customcss);

	//Set page background color
    if (!empty($theme->settings->backgroundcolor)) {
        $backgroundcolor = $theme->settings->backgroundcolor;
    } else {
        $backgroundcolor = null;
    }
    $css = androtheme_set_backgroundcolor($css, $backgroundcolor);
    
	//Set header background color
    if (!empty($theme->settings->headerbgcolor)) {
        $headerbgcolor = $theme->settings->headerbgcolor;
    } else {
        $headerbgcolor = null;
    }
    $css = androtheme_set_headerbgcolor($css, $headerbgcolor);
	
	//Set page nav (breadcrumb) text color
    if (!empty($theme->settings->pagenavtextcolor)) {
        $pagenavtextcolor = $theme->settings->pagenavtextcolor;
    } else {
        $pagenavtextcolor = null;
    }
    $css = androtheme_set_pagenavtextcolor($css, $pagenavtextcolor);
	
	//Set page nav (breadcrumb) background color
    if (!empty($theme->settings->pagenavbgcolor)) {
        $pagenavbgcolor = $theme->settings->pagenavbgcolor;
    } else {
        $pagenavbgcolor = null;
    }
    $css = androtheme_set_pagenavbgcolor($css, $pagenavbgcolor);
	
    //Set block's text header color
    if (!empty($theme->settings->blockheadertextcolor)) {
        $blockheadertextcolor = $theme->settings->blockheadertextcolor;
    } else {
        $blockheadertextcolor = null;
    }
    $css = androtheme_set_blockheadertextcolor($css, $blockheadertextcolor);
    
    //Set block's header background colour
	if (!empty($theme->settings->blockheaderbgcolor)) {
        $blockheaderbgcolor = $theme->settings->blockheaderbgcolor;
    } else {
        $blockheaderbgcolor = null;
    }
    $css = androtheme_set_blockheaderbgcolor($css, $blockheaderbgcolor);
	
	//Set block's background colour
    if (!empty($theme->settings->blockbgcolor)) {
        $blockbgcolor = $theme->settings->blockbgcolor;
    } else {
        $blockbgcolor = null;
    }
    $css = androtheme_set_blockbgcolor($css, $blockbgcolor);
		
    //Set link color
    if (!empty($theme->settings->linkcolor)) {
        $linkcolor = $theme->settings->linkcolor;
    } else {
        $linkcolor = null;
    }
    $css = androtheme_set_linkcolor($css, $linkcolor);
	
    //Set visited link color
    if (!empty($theme->settings->linkvisitedcolor)) {
        $linkvisitedcolor = $theme->settings->linkvisitedcolor;
    } else {
        $linkvisitedcolor = null;
    }
    $css = androtheme_set_linkvisitedcolor($css, $linkvisitedcolor);
	
	//Set button color
    if (!empty($theme->settings->buttoncolor)) {
        $buttoncolor = $theme->settings->buttoncolor;
    } else {
        $buttoncolor = null;
    }
    $css = androtheme_set_buttoncolor($css, $buttoncolor);
	
    return $css;
}

/********************************************
SET
*********************************************/

function theme_androtheme_set_logo($css, $logo) {
    global $OUTPUT;
    $tag = '[[setting:logo]]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = $OUTPUT->pix_url('logo','theme');
        //$replacement = 'androtheme/pix/logo.png';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_androtheme_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM && ($filearea === 'logo' || $filearea === 'favicon')) {
        $theme = theme_config::load('androtheme');
        return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}

//Sets the background colour variable in CSS
function androtheme_set_backgroundcolor($css, $backgroundcolor) {
        $tag = '[[setting:backgroundcolor]]';
        $replacement = $backgroundcolor;
        if (is_null($replacement)) {
                $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Sets the text header color in the block
function androtheme_set_blockheadertextcolor($css, $blockheadertextcolor) {
        $tag = '[[setting:blockheadertextcolor]]';
        $replacement = $blockheadertextcolor;
        if (is_null($replacement)) {
            $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Sets the header background color
function androtheme_set_headerbgcolor($css, $headerbgcolor) {
        $tag = '[[setting:headerbgcolor]]';
        $replacement = $headerbgcolor;
        if (is_null($replacement)) {
            $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Sets the header background of the block
function androtheme_set_blockheaderbgcolor($css, $blockheaderbgcolor) {
        $tag = '[[setting:blockheaderbgcolor]]';
        $replacement = $blockheaderbgcolor;
        if (is_null($replacement)) {
            $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Sets the background of the block
function androtheme_set_blockbgcolor($css, $blockbgcolor) {
        $tag = '[[setting:blockbgcolor]]';
        $replacement = $blockbgcolor;
        if (is_null($replacement)) {
            $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Sets the breadcrumb's text color
function androtheme_set_pagenavtextcolor($css, $pagenavtextcolor) {
        $tag = '[[setting:pagenavtextcolor]]';
        $replacement = $pagenavtextcolor;
        if (is_null($replacement)) {
            $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Sets the breadcrumb's background color
function androtheme_set_pagenavbgcolor($css, $pagenavbgcolor) {
        $tag = '[[setting:pagenavbgcolor]]';
        $replacement = $pagenavbgcolor;
        if (is_null($replacement)) {
            $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Sets the primary dark colour variable in CSS
function androtheme_set_linkcolor($css, $linkcolor) {
        $tag = '[[setting:linkcolor]]';
        $replacement = $linkcolor;
        if (is_null($replacement)) {
            $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Sets the primary accent colour variable in CSS
function androtheme_set_linkvisitedcolor($css, $linkvisitedcolor) {
        $tag = '[[setting:linkvisitedcolor]]';
        $replacement = $linkvisitedcolor;
        if (is_null($replacement)) {
            $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Sets the button color
function androtheme_set_buttoncolor($css, $buttoncolor) {
        $tag = '[[setting:buttoncolor]]';
        $replacement = $buttoncolor;
        if (is_null($replacement)) {
            $replacement = '';
        }
        $css = str_replace($tag, $replacement, $css);
        return $css;
}

//Adds any custom CSS to the CSS before it is cached.
function theme_androtheme_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_androtheme_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->bannertext = '';
    if (!empty($page->theme->settings->bannertext)) {
        $return->bannertext = '<div class="bannertext text-right">'.$page->theme->settings->bannertext.'</div>';
    }

    $return->footnote = '';
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = $page->theme->settings->footnote;
    }

    return $return;
}