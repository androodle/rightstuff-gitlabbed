<?php

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

        // Logo file setting.
        $name = 'theme_nswhealth/logo';
        $title = get_string('logo','theme_nswhealth');
        $description = get_string('logodesc', 'theme_nswhealth');
        //$default = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        // Favicon file setting.
        $name = 'theme_nswhealth/favicon';
        $title = new lang_string('favicon', 'theme_nswhealth');
        $description = new lang_string('favicondesc', 'theme_nswhealth');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon', 0, array('accepted_types' => '.ico'));
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Banner header text setting
        $name = 'theme_nswhealth/bannertext';
        $title = get_string('bannertext', 'theme_nswhealth');
        $description = get_string('bannertextdesc', 'theme_nswhealth');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Custom CSS file
        $name = 'theme_nswhealth/customcss';
        $title = get_string('customcss', 'theme_nswhealth');
        $description = get_string('customcssdesc', 'theme_nswhealth');
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Footnote setting
        $name = 'theme_nswhealth/footnote';
        $title = get_string('footnote', 'theme_nswhealth');
        $description = get_string('footnotedesc', 'theme_nswhealth');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
}