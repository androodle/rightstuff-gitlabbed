<footer id="page-footer" class="row-fluid">
    <div id="footnotewrapper" class="container-fluid">
        <div class="logos-footer">
            <a href="http://www.nrl.com/" target="_blank"><img src="<?php echo $OUTPUT->pix_url('logo_nrl', 'theme')?>" alt="NRL" /></a>
            <a href="http://nsw.netball.com.au/" target="_blank"><img src="<?php echo $OUTPUT->pix_url('logo_netballnsw', 'theme')?>" alt="Netball NSW" /></a>
            <a href="http://www.afl.com.au/" target="_blank"><img src="<?php echo $OUTPUT->pix_url('logo_afl', 'theme')?>" alt="AFL" /></a>
        </div>
        <div class="footer-text">
            <span class="year">&copy;<?php echo date('Y')?></span>
            <?php echo$html->footnote; ?>
        </div>
        <div class="helplink"><?php echo $OUTPUT->page_doc_link(); ?></div>
    </div> 
</footer>