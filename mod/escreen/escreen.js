function set_item_focus(itemid) {
    var item = document.getElementById(itemid);
    if(item){
        item.focus();
    }
}

function escreenGo2delete(form) {
    form.action = M.cfg.wwwroot+'/mod/escreen/delete_completed.php';
    form.submit();
}

function setcourseitemfilter(item, item_typ) {
    document.report.courseitemfilter.value = item;
    document.report.courseitemfiltertyp.value = item_typ;
    document.report.submit();
}


M.mod_escreen = {};

M.mod_escreen.init_sendmessage = function(Y) {
    Y.on('click', function(e) {
        Y.all('input.usercheckbox').each(function() {
            this.set('checked', 'checked');
        });
    }, '#checkall');

    Y.on('click', function(e) {
        Y.all('input.usercheckbox').each(function() {
            this.set('checked', '');
        });
    }, '#checknone');
};

M.mod_escreen.init = function(Y) {
	
	Y.all('.escreen_complete_depend').each(function() {
		
		var inputs = this.all('input');
		if (!inputs) {
			return;
		}
		
		var can_hide = true;
		for (var i = 0; i < inputs.size(); i++) {
			var type = inputs.item(i).getAttribute('type');
			if (type == 'radio' || type == 'checkbox') {
				if (inputs.item(i).get('checked')) {
					can_hide = false;
					break;
				}
			} else if (type == 'text') {
				can_hide = !inputs.item(i).getAttribute('value');
			}
		}
		
		if (can_hide) {
			this.hide();
		}
	});
	
	Y.on('click', function() {
		
		var name = this.getAttribute('name');
		if (name.indexOf('_') === -1) {
			return;
		}
		
		var name_arr = name.split('_');
		var id = parseInt(name_arr[1]);
		if (typeof id === 'undefined' || typeof depends[id] === 'undefined' || id <= 0) {
			return;
		}
		
		Y.one('.item_' + id).hide();
		var this_label = this.ancestor().ancestor().one('label').get('text').trim();
		if (this_label == depends[id]) {
			Y.one('.item_' + id).show();
		}
	}, '#escreen_form input');
	
	if (typeof popups !== 'undefined' && popups.length) {
		for (var i = 0; i < popups.length; i++) {
			M.mod_escreen.popup(popups[i]);
		}
	}
};

M.mod_escreen.popup = function(text) {
	alert(text);
}
