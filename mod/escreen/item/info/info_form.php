<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once($CFG->dirroot.'/mod/escreen/item/escreen_item_form_class.php');

class escreen_info_form extends escreen_item_form {
    protected $type = "info";

    public function definition() {

        $item = $this->_customdata['item'];
        $common = $this->_customdata['common'];
        $positionlist = $this->_customdata['positionlist'];
        $position = $this->_customdata['position'];

        $mform =& $this->_form;

        $mform->addElement('header', 'general', get_string($this->type, 'escreen'));
        $mform->addElement('hidden', 'required', 0);
        $mform->setType('required', PARAM_INT);

        $mform->addElement('editor',
                            'name_editor',
                            get_string('item_name', 'escreen'));
		$mform->setType('name_editor', PARAM_RAW);
		
        $mform->addElement('text',
                            'label',
                            get_string('item_label', 'escreen'),
                            array('size'=>ESCREEN_ITEM_LABEL_TEXTBOX_SIZE, 'maxlength'=>255));

        $options=array();
        $options[1]  = get_string('responsetime', 'escreen');
        $options[2]  = get_string('course');
        $options[3]  = get_string('coursecategory');
        $this->infotype = &$mform->addElement('select',
                                              'presentation',
                                              get_string('infotype', 'escreen'),
                                              $options);

        parent::definition();
        $this->set_data($item);

    }
	
	public function set_data($item) {
		if (!empty($item->name)) {
			$item->name_editor['text'] = $item->name;
		}
		return parent::set_data($item);
	}
	
    public function get_data() {
        if (!$item = parent::get_data()) {
            return false;
        }
		$item->name = $item->name_editor['text'];
        return $item;
    }	

}

