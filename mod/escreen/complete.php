<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * prints the form so the user can fill out the escreen
 *
 * @author Andreas Grabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package mod_escreen
 */

require_once("../../config.php");
require_once("lib.php");
require_once($CFG->libdir . '/completionlib.php');

escreen_init_escreen_session();

$id = required_param('id', PARAM_INT);
$completedid = optional_param('completedid', false, PARAM_INT);
$preservevalues  = optional_param('preservevalues', 0,  PARAM_INT);
$courseid = optional_param('courseid', false, PARAM_INT);
$gopage = optional_param('gopage', -1, PARAM_INT);
$lastpage = optional_param('lastpage', false, PARAM_INT);
$startitempos = optional_param('startitempos', 0, PARAM_INT);
$lastitempos = optional_param('lastitempos', 0, PARAM_INT);

$highlightrequired = false;

if (($formdata = data_submitted()) AND !confirm_sesskey()) {
    print_error('invalidsesskey');
}

//if the use hit enter into a textfield so the form should not submit
if (isset($formdata->sesskey) &&
    !isset($formdata->savevalues) &&
    !isset($formdata->gonextpage) &&
    !isset($formdata->gopreviouspage)) {

    $gopage = $formdata->lastpage;
}

if (isset($formdata->savevalues)) {
    $savevalues = true;
} else {
    $savevalues = false;
}

if ($gopage < 0 && !$savevalues) {
    if (isset($formdata->gonextpage)) {
        $gopage = $lastpage + 1;
        $gonextpage = true;
        $gopreviouspage = false;
    } else if (isset($formdata->gopreviouspage)) {
        $gopage = $lastpage - 1;
        $gonextpage = false;
        $gopreviouspage = true;
    } else {
        print_error('missingparameter');
    }
} else {
    $gonextpage = $gopreviouspage = false;
}

if (! $cm = get_coursemodule_from_id('escreen', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id"=>$cm->course))) {
    print_error('coursemisconf');
}

if (! $escreen = $DB->get_record("escreen", array("id"=>$cm->instance))) {
    print_error('invalidcoursemodule');
}

$context = context_module::instance($cm->id);

$escreen_complete_cap = false;

if (has_capability('mod/escreen:complete', $context)) {
    $escreen_complete_cap = true;
}

//check whether the escreen is located and! started from the mainsite
if ($course->id == SITEID AND !$courseid) {
    $courseid = SITEID;
}

//check whether the escreen is mapped to the given courseid
if ($course->id == SITEID AND !has_capability('mod/escreen:edititems', $context)) {
    if ($DB->get_records('escreen_sitecourse_map', array('escreenid'=>$escreen->id))) {
        $params = array('escreenid'=>$escreen->id, 'courseid'=>$courseid);
        if (!$DB->get_record('escreen_sitecourse_map', $params)) {
            print_error('notavailable', 'escreen');
        }
    }
}

if ($escreen->anonymous != ESCREEN_ANONYMOUS_YES) {
    if ($course->id == SITEID) {
        require_login($course, true);
    } else {
        require_login($course, true, $cm);
    }
} else {
    if ($course->id == SITEID) {
        require_course_login($course, true);
    } else {
        require_course_login($course, true, $cm);
    }
}

//check whether the given courseid exists
if ($courseid AND $courseid != SITEID) {
    if ($course2 = $DB->get_record('course', array('id'=>$courseid))) {
        require_course_login($course2); //this overwrites the object $course :-(
        $course = $DB->get_record("course", array("id"=>$cm->course)); // the workaround
    } else {
        print_error('invalidcourseid');
    }
}

if (!$escreen_complete_cap) {
    print_error('error');
}

// Mark activity viewed for completion-tracking
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

/// Print the page header
$strescreens = get_string("modulenameplural", "escreen");
$strescreen  = get_string("modulename", "escreen");

if ($course->id == SITEID) {
    $PAGE->set_cm($cm, $course); // set's up global $COURSE
    $PAGE->set_pagelayout('incourse');
}

$PAGE->navbar->add(get_string('escreen:complete', 'escreen'));
$urlparams = array('id'=>$cm->id, 'gopage'=>$gopage, 'courseid'=>$course->id);
$PAGE->set_url('/mod/escreen/complete.php', $urlparams);
$PAGE->set_heading($course->fullname);
$PAGE->set_title($escreen->name);
$PAGE->requires->js('/mod/escreen/escreen.js');
$PAGE->requires->js_init_call('M.mod_escreen.init');
echo $OUTPUT->header();

//ishidden check.
//escreen in courses
if ((empty($cm->visible) AND
        !has_capability('moodle/course:viewhiddenactivities', $context)) AND
        $course->id != SITEID) {
    notice(get_string("activityiscurrentlyhidden"));
}

//ishidden check.
//escreen on mainsite
if ((empty($cm->visible) AND
        !has_capability('moodle/course:viewhiddenactivities', $context)) AND
        $courseid == SITEID) {
    notice(get_string("activityiscurrentlyhidden"));
}

//check, if the escreen is open (timeopen, timeclose)
$checktime = time();
$escreen_is_closed = ($escreen->timeopen > $checktime) ||
                      ($escreen->timeclose < $checktime &&
                            $escreen->timeclose > 0);

if ($escreen_is_closed) {
    echo $OUTPUT->heading(format_string($escreen->name));
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    echo $OUTPUT->notification(get_string('escreen_is_not_open', 'escreen'));
    echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
    echo $OUTPUT->box_end();
    echo $OUTPUT->footer();
    exit;
}

$messages = array();

//additional check for multiple-submit (prevent browsers back-button).
//the main-check is in view.php
$escreen_can_submit = true;
if ($escreen->multiple_submit == 0 ) {
    if (escreen_is_already_submitted($escreen->id, $courseid)) {
        $escreen_can_submit = false;
    }
}
if ($escreen_can_submit) {
    //preserving the items
    if ($preservevalues == 1) {
        if (!isset($SESSION->escreen->is_started) OR !$SESSION->escreen->is_started == true) {
            print_error('error', '', $CFG->wwwroot.'/course/view.php?id='.$course->id);
        }
        // Check if all required items have a value.
        if (escreen_check_values($startitempos, $lastitempos)) {
            $userid = $USER->id; //arb
            if ($completedid = escreen_save_values($USER->id, true)) {
                if (!$gonextpage && !$gopreviouspage) {
                    $preservevalues = false;// It can be stored.
                }

            } else {
                $savereturn = 'failed';
                if (isset($lastpage)) {
                    $gopage = $lastpage;
                } else {
                    print_error('missingparameter');
                }
            }
        } else {
            $savereturn = 'missing';
            $highlightrequired = true;
            if (isset($lastpage)) {
                $gopage = $lastpage;
            } else {
                print_error('missingparameter');
            }

        }
    }

    //saving the items
    if ($savevalues && !$preservevalues) {
        //exists there any pagebreak, so there are values in the escreen_valuetmp
        $userid = $USER->id; //arb
		
		// check the submitted values to see if we have to display
		// any messages based on them
		$completedvalues = escreen_get_completed_values($escreen->id, true, $courseid);
		$messages = escreen_get_messages($escreen, $completedvalues);

        if ($escreen->anonymous == ESCREEN_ANONYMOUS_NO) {
            $escreencompleted = escreen_get_current_completed($escreen->id, false, $courseid);
        } else {
            $escreencompleted = false;
        }
        $params = array('id' => $completedid);
        $escreencompletedtmp = $DB->get_record('escreen_completedtmp', $params);
        //fake saving for switchrole
        $is_switchrole = escreen_check_is_switchrole();
        if ($is_switchrole) {
            $savereturn = 'saved';
            escreen_delete_completedtmp($completedid);
        } else {
            $new_completed_id = escreen_save_tmp_values($escreencompletedtmp,
                                                         $escreencompleted,
                                                         $userid);
            if ($new_completed_id) {
                $savereturn = 'saved';
                if ($escreen->anonymous == ESCREEN_ANONYMOUS_NO) {
                    escreen_send_email($cm, $escreen, $course, $userid);
                } else {
                    escreen_send_email_anonym($cm, $escreen, $course, $userid);
                }
                //tracking the submit
                $tracking = new stdClass();
                $tracking->userid = $USER->id;
                $tracking->escreen = $escreen->id;
                $tracking->completed = $new_completed_id;
                $DB->insert_record('escreen_tracking', $tracking);
                unset($SESSION->escreen->is_started);

                // Update completion state
                $completion = new completion_info($course);
                if ($completion->is_enabled($cm) && $escreen->completionsubmit) {
                    $completion->update_state($cm, COMPLETION_COMPLETE);
                }

            } else {
                $savereturn = 'failed';
            }
        }

    }


    if ($allbreaks = escreen_get_all_break_positions($escreen->id)) {
        if ($gopage <= 0) {
            $startposition = 0;
        } else {
            if (!isset($allbreaks[$gopage - 1])) {
                $gopage = count($allbreaks);
            }
            $startposition = $allbreaks[$gopage - 1];
        }
        $ispagebreak = true;
    } else {
        $startposition = 0;
        $newpage = 0;
        $ispagebreak = false;
    }

    //get the escreenitems after the last shown pagebreak
    $select = 'escreen = ? AND position > ?';
    $params = array($escreen->id, $startposition);
    $escreenitems = $DB->get_records_select('escreen_item', $select, $params, 'position');

    //get the first pagebreak
    $params = array('escreen' => $escreen->id, 'typ' => 'pagebreak');
    if ($pagebreaks = $DB->get_records('escreen_item', $params, 'position')) {
        $pagebreaks = array_values($pagebreaks);
        $firstpagebreak = $pagebreaks[0];
    } else {
        $firstpagebreak = false;
    }
    $maxitemcount = $DB->count_records('escreen_item', array('escreen'=>$escreen->id));

    //get the values of completeds before done. Anonymous user can not get these values.
    if ((!isset($SESSION->escreen->is_started)) AND
                          (!isset($savereturn)) AND
                          ($escreen->anonymous == ESCREEN_ANONYMOUS_NO)) {

        $escreencompletedtmp = escreen_get_current_completed($escreen->id, true, $courseid);
        if (!$escreencompletedtmp) {
            $escreencompleted = escreen_get_current_completed($escreen->id, false, $courseid);
            if ($escreencompleted) {
                //copy the values to escreen_valuetmp create a completedtmp
                $escreencompletedtmp = escreen_set_tmp_values($escreencompleted);
            }
        }
    } else {
        $escreencompletedtmp = escreen_get_current_completed($escreen->id, true, $courseid);
    }

    /// Print the main part of the page
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    $analysisurl = new moodle_url('/mod/escreen/analysis.php', array('id'=>$id));
    if ($courseid > 0) {
        $analysisurl->param('courseid', $courseid);
    }
    echo $OUTPUT->heading(format_string($escreen->name));

	$completedvalues = escreen_get_completed_values($escreen->id, true, $courseid);
	$popups = escreen_get_popups($escreen, $completedvalues);
	if (!empty($popups)) {
		echo '<script type="text/javascript">';
		echo 'popups = [];';
		foreach ($popups as $pkey => $popup) {
			if (!isset($_SESSION['escreen_popups_displayed'][$pkey])) {
				echo "popups.push('". addslashes($popup) ."');\n";
				$_SESSION['escreen_popups_displayed'][$pkey] = $pkey;
			}
		}
		echo '</script>';
	}

    if ( (intval($escreen->publish_stats) == 1) &&
            ( has_capability('mod/escreen:viewanalysepage', $context)) &&
            !( has_capability('mod/escreen:viewreports', $context)) ) {

        $params = array('userid' => $USER->id, 'escreen' => $escreen->id);
        if ($multiple_count = $DB->count_records('escreen_tracking', $params)) {
            echo $OUTPUT->box_start('mdl-align');
            echo '<a href="'.$analysisurl->out().'">';
            echo get_string('completed_escreens', 'escreen').'</a>';
            echo $OUTPUT->box_end();
        }
    }

    if (isset($savereturn) && $savereturn == 'saved') {
        if ($escreen->page_after_submit) {

            require_once($CFG->libdir . '/filelib.php');

            $page_after_submit_output = file_rewrite_pluginfile_urls($escreen->page_after_submit,
                                                                    'pluginfile.php',
                                                                    $context->id,
                                                                    'mod_escreen',
                                                                    'page_after_submit',
                                                                    0);

            echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
			
			if (!empty($messages)) {
				foreach ($messages as $message) {
					echo format_text($message, FORMAT_HTML, array('overflowdiv' => true));
					echo '<hr>';
				}
			}
			
            echo format_text($page_after_submit_output,
                             $escreen->page_after_submitformat,
                             array('overflowdiv' => true));
			
            echo $OUTPUT->box_end();
        } else {
            echo '<p align="center">';
            echo '<b><font color="green">';
            echo get_string('entries_saved', 'escreen');
            echo '</font></b>';
            echo '</p>';
            if ( intval($escreen->publish_stats) == 1) {
                echo '<p align="center"><a href="'.$analysisurl->out().'">';
                echo get_string('completed_escreens', 'escreen').'</a>';
                echo '</p>';
            }
        }

        if ($escreen->site_after_submit) {
            $url = escreen_encode_target_url($escreen->site_after_submit);
        } else {
            if ($courseid) {
                if ($courseid == SITEID) {
                    $url = $CFG->wwwroot;
                } else {
                    $url = $CFG->wwwroot.'/course/view.php?id='.$courseid;
                }
            } else {
                if ($course->id == SITEID) {
                    $url = $CFG->wwwroot;
                } else {
                    $url = $CFG->wwwroot.'/course/view.php?id='.$course->id;
                }
            }
        }
        echo $OUTPUT->continue_button($url);
    } else {
        if (isset($savereturn) && $savereturn == 'failed') {
            echo $OUTPUT->box_start('mform');
            echo '<span class="error">'.get_string('saving_failed', 'escreen').'</span>';
            echo $OUTPUT->box_end();
        }

        if (isset($savereturn) && $savereturn == 'missing') {
            echo $OUTPUT->box_start('mform');
            echo '<span class="error">'.get_string('saving_failed_because_missing_or_false_values', 'escreen').'</span>';
            echo $OUTPUT->box_end();
        }

        //print the items
        if (is_array($escreenitems)) {
            echo $OUTPUT->box_start('escreen_form');
            echo '<form action="complete.php" class="mform" method="post" onsubmit=" " id="escreen_form">';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
            echo $OUTPUT->box_start('escreen_anonymousinfo');
            switch ($escreen->anonymous) {
                case ESCREEN_ANONYMOUS_YES:
                    echo '<input type="hidden" name="anonymous" value="1" />';
                    $inputvalue = 'value="'.ESCREEN_ANONYMOUS_YES.'"';
                    echo '<input type="hidden" name="anonymous_response" '.$inputvalue.' />';
                    //echo get_string('mode', 'escreen').': '.get_string('anonymous', 'escreen');
                    break;
                case ESCREEN_ANONYMOUS_NO:
                    echo '<input type="hidden" name="anonymous" value="0" />';
                    $inputvalue = 'value="'.ESCREEN_ANONYMOUS_NO.'"';
                    echo '<input type="hidden" name="anonymous_response" '.$inputvalue.' />';
                    //echo get_string('mode', 'escreen').': ';
                    //echo get_string('non_anonymous', 'escreen');
                    break;
            }
            echo $OUTPUT->box_end();
            //check, if there exists required-elements
            $params = array('escreen' => $escreen->id, 'required' => 1);
            $countreq = $DB->count_records('escreen_item', $params);
            if ($countreq > 0) {
                echo '<span class="fdescription required">';
                echo get_string('somefieldsrequired', 'form', '<img alt="'.get_string('requiredelement', 'form').
                    '" src="'.$OUTPUT->pix_url('req') .'" class="req" />');
                echo '</span>';
            }
            echo $OUTPUT->box_start('escreen_items');

            unset($startitem);
            $select = 'escreen = ? AND hasvalue = 1 AND position < ?';
            $params = array($escreen->id, $startposition);
            $itemnr = $DB->count_records_select('escreen_item', $select, $params);
            $lastbreakposition = 0;
            $align = right_to_left() ? 'right' : 'left';
			
			$depends = array();

            foreach ($escreenitems as $escreenitem) {
                if (!isset($startitem)) {
                    //avoid showing double pagebreaks
                    if ($escreenitem->typ == 'pagebreak') {
                        continue;
                    }
                    $startitem = $escreenitem;
                }

				// we DO want to display dependent items
				$dependstyleadd = '';
                if ($escreenitem->dependitem > 0 && !empty($escreencompletedtmp)) {
                    //chech if the conditions are ok
                    $fb_compare_value = escreen_compare_item_value($escreencompletedtmp->id,
                                                                    $escreenitem->dependitem,
                                                                    $escreenitem->dependvalue,
                                                                    true);
                    if (!isset($escreencompletedtmp->id) || !$fb_compare_value) {
						//$lastitem = $escreenitem;
                        //$lastbreakposition = $escreenitem->position;
						//$dependstyleadd = ' depend_show';
                        //continue;
                    }
                }

				$dependstyle = '';
                if ($escreenitem->dependitem > 0) {
				    $dependstyle = ' item_' .$escreenitem->dependitem. ' escreen_complete_depend'. $dependstyleadd;
				    $depends[] = 'depends[' .$escreenitem->dependitem. "] = '". $escreenitem->dependvalue ."';";
                }

                echo $OUTPUT->box_start('escreen_item_box_'.$align.$dependstyle);
                $value = '';
                //get the value
                $frmvaluename = $escreenitem->typ . '_'. $escreenitem->id;
                if (isset($savereturn)) {
                    $value = isset($formdata->{$frmvaluename}) ? $formdata->{$frmvaluename} : null;
                    $value = escreen_clean_input_value($escreenitem, $value);
                } else {
                    if (isset($escreencompletedtmp->id)) {
                        $value = escreen_get_item_value($escreencompletedtmp->id,
                                                         $escreenitem->id,
                                                         true);
                    }
                }
                if ($escreenitem->hasvalue == 1 && $escreen->autonumbering) {
                    $itemnr++;
                    echo $OUTPUT->box_start('escreen_item_number_'.$align);
                    echo $itemnr . '. &nbsp;';
                    echo $OUTPUT->box_end();
                }
                if ($escreenitem->typ != 'pagebreak') {
                    echo $OUTPUT->box_start('box generalbox boxalign_'.$align);
                    escreen_print_item_complete($escreenitem, $value, $highlightrequired);
                    echo $OUTPUT->box_end();
                }

                echo $OUTPUT->box_end();

                $lastbreakposition = $escreenitem->position; //last item-pos (item or pagebreak)
                if ($escreenitem->typ == 'pagebreak') {
                    break;
                } else {
                    $lastitem = $escreenitem;
                }
            }
            echo $OUTPUT->box_end();
			
			if (!empty($depends)) {
				// init JS variables for dependable items
				echo '<script type="text/javascript">';
				echo 'depends = [];';
				echo implode("\n", $depends);
				echo '</script>';
			}
			
            echo '<input type="hidden" name="id" value="'.$id.'" />';
            echo '<input type="hidden" name="escreenid" value="'.$escreen->id.'" />';
            echo '<input type="hidden" name="lastpage" value="'.$gopage.'" />';
            if (isset($escreencompletedtmp->id)) {
                $inputvalue = 'value="'.$escreencompletedtmp->id.'"';
            } else {
                $inputvalue = 'value=""';
            }
            echo '<input type="hidden" name="completedid" '.$inputvalue.' />';
            echo '<input type="hidden" name="courseid" value="'. $courseid . '" />';
            echo '<input type="hidden" name="preservevalues" value="1" />';
            if (isset($startitem)) {
                echo '<input type="hidden" name="startitempos" value="'.$startitem->position.'" />';
                echo '<input type="hidden" name="lastitempos" value="'.$lastitem->position.'" />';
            }

            if ( $ispagebreak AND $lastbreakposition > $firstpagebreak->position) {
                $inputvalue = 'value="'.get_string('previous_page', 'escreen').'"';
                echo '<input name="gopreviouspage" type="submit" '.$inputvalue.' />';
            }
            if ($lastbreakposition < $maxitemcount) {
                $inputvalue = 'value="'.get_string('next_page', 'escreen').'"';
                echo '<input name="gonextpage" type="submit" '.$inputvalue.' />';
            }
            if ($lastbreakposition >= $maxitemcount) { //last page
                $inputvalue = 'value="'.get_string('save_entries', 'escreen').'"';
                echo '<input name="savevalues" type="submit" '.$inputvalue.' />';
            }

            echo '</form>';
            echo $OUTPUT->box_end();

            echo $OUTPUT->box_start('escreen_complete_cancel');
            if ($courseid) {
                $action = 'action="'.$CFG->wwwroot.'/course/view.php?id='.$courseid.'"';
            } else {
                if ($course->id == SITEID) {
                    $action = 'action="'.$CFG->wwwroot.'"';
                } else {
                    $action = 'action="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'"';
                }
            }
            echo '<form '.$action.' method="post" onsubmit=" ">';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
            echo '<input type="hidden" name="courseid" value="'. $courseid . '" />';
            echo '<button type="submit">'.get_string('cancel').'</button>';
            echo '</form>';
            echo $OUTPUT->box_end();
            $SESSION->escreen->is_started = true;
        }
    }
} else {
    echo $OUTPUT->heading(format_string($escreen->name));
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    echo $OUTPUT->notification(get_string('this_escreen_is_already_submitted', 'escreen'));
    echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
    echo $OUTPUT->box_end();
}
/// Finish the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

echo $OUTPUT->footer();
