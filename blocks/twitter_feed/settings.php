<?php

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

$settings->add(new admin_setting_configtext('block_twitter_feed_title', get_string('twitter_feed_title', 'block_twitter_feed'),
                    '', PARAM_TEXT));

    $settings->add(new admin_setting_configtext('block_twitter_username', get_string('twitter_feed_username', 'block_twitter_feed'),
                   get_string('twitter_feed_eg', 'block_twitter_feed'), '', PARAM_TEXT));
	
	$settings->add(new admin_setting_configtext('block_twitter_no_feeds', get_string('nosoffeeds', 'block_twitter_feed'),
	                   get_string('nosoffeeds', 'block_twitter_feed'), 5, PARAM_NUMBER));
					   
}

