<?php
/** 
 * Androgogic Support Block: Version
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$plugin->version = 2013060602;
$plugin->requires = 2010112400; // Indicate we need at least Moodle 2.0

// End of blocks/androgogic_support/version.php