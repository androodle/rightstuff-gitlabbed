<?php

/** 
 * Action Plans Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     11/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class strategy_edit_form extends moodleform {
protected $strategy;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.* , mdl_ap_objectives.name as objective 
from mdl_ap_strategies a 
LEFT JOIN mdl_ap_objectives  on a.objective_id = mdl_ap_objectives.id
where a.id = {$_REQUEST['id']} ";
$strategy = $DB->get_record_sql($q);
}
else{
$strategy = $this->_customdata['$strategy']; // this contains the data of this form
}
$tab = 'strategy_new'; // from whence we were called
if (!empty($strategy->id)) {
$tab = 'strategy_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_action_plans'), array('size'=>150));
$mform->setType('name', PARAM_RAW);
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 50 characters', 'maxlength', 255, 'client');

//objective_id
$options = $DB->get_records_menu('ap_objectives',null,'name','id,name'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'objective_id', get_string('objective','block_action_plans'), $options);
$mform->addRule('objective_id', get_string('required'), 'required', null, 'server');
//set values if we are in edit mode
if (!empty($strategy->id) && isset($_GET['id'])) {
$mform->setConstant('name', $strategy->name);
$mform->setConstant('objective_id', $strategy->objective_id);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->setType('tab', PARAM_TEXT);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
$mform->setType('id', PARAM_INT);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
$mform->setType('id', PARAM_INT);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
