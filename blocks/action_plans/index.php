<?php

/**
 * Action Plans Block: Index 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 * */
require_once('../../config.php');
require_once('lib.php');
global $CFG, $USER, $DB, $PAGE, $OUTPUT;
//expected params
$currenttab = optional_param('tab', 'club_strategy_search', PARAM_FILE);
$download = optional_param('download', '', PARAM_RAW);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url("$CFG->wwwroot/blocks/action_plans/index.php", array('tab' => $currenttab));
$PAGE->set_title(get_string($currenttab, 'block_action_plans'));
$PAGE->set_heading(get_string('plugintitle', 'block_action_plans'));
$PAGE->navbar->add(get_string('action_plan_search', 'block_action_plans'), "$CFG->wwwroot/blocks/action_plans/index.php", 'misc');
require_login();
// if we are getting a post from club_strategy_edit then we may be in the middle of doing a redirect. If so we don't want the header
$redirect = false;
if ($currenttab == 'club_strategy_edit' && isset($_POST['status'])){  
    if($_POST['status'] != 'complete' || ($_POST['status'] == 'complete' && isset($_POST['tell_us_how']))){
        $redirect = true;
    }
}
if ($download == '' && !$redirect) {
    echo $OUTPUT->header();
    if (has_capability('block/action_plans:edit', $context)) {
//show tabs
        include('tabs.php');
    }
}
// include current page
include $currenttab . '.php';
if ($download == '') {
    echo $OUTPUT->footer();
}


// End of blocks/action_plans/index.php