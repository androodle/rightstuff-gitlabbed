<?php

/** 
 * Action Plans Block: Delete object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     21/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Delete one of the trigger_points
 *  
 **/

require_capability('block/action_plans:edit', $context);
$id = required_param('id', PARAM_INT);
$DB->delete_records('ap_trigger_points',array('id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_action_plans'), 'notifysuccess');

