<?php

/** 
 * Action Plans Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     18/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the goals
 *  
 **/

global $OUTPUT;
require_capability('block/action_plans:edit', $context);
require_once('goal_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , mdl_ap_action_plans.name as action_plan 
from mdl_ap_goals a 
LEFT JOIN mdl_ap_action_plans  on a.action_plan_id = mdl_ap_action_plans.id
where a.id = $id ";
$goal = $DB->get_record_sql($q);
$mform = new goal_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$DB->update_record('ap_goals',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_action_plans'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('goal_edit', 'block_action_plans'));
$mform->display();
}

