<?php

/** 
 * Action Plans Block: Search 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     02/06/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search club_action_plans
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 **/

//params
$sort   = optional_param('sort', 'mdl_ap_clubs.name', PARAM_RAW);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 50, PARAM_INT); 
$search = optional_param('search', '', PARAM_TEXT);
$tab    = optional_param('tab', 'club_action_plan_search', PARAM_FILE);
$debug   = optional_param('debug', 0, PARAM_INT);
$ap_action_plans_id = optional_param('ap_action_plans_id', 0, PARAM_INT); 
$ap_clubs_id = optional_param('ap_clubs_id', 0, PARAM_INT); 
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort','dir','page','perpage','search','tab','ap_action_plans_id','ap_clubs_id'));
// prepare columns for results table
$columns = array(
"action_plan",
"club",
"target_date",
);
foreach ($columns as $column) {
$string[$column] = get_string("$column",'block_action_plans');
if ($sort != $column) {
$columnicon = '';
$columndir = 'ASC';
} else {
$columndir = $dir == 'ASC' ? 'DESC':'ASC';
$columnicon = $dir == 'ASC' ? 'down':'up';
}
if($column != 'details'){
$$column = "<a href='$PAGE->url&dir=$columndir&sort=$column'>$string[$column]</a>";
}
else{
 $$column = $string[$column];
}
}
//figure out the and clause from what has been submitted
$and = '';
if($search != ''){
    $and .= " and mdl_ap_clubs.name LIKE '%$search%' ";
} 
//are we filtering on ap_action_plans?
if($ap_action_plans_id > 0){ 
$and .= " and mdl_ap_action_plans.id = $ap_action_plans_id ";
} 
//are we filtering on ap_clubs?
if($ap_clubs_id > 0){ 
$and .= " and mdl_ap_clubs.id = $ap_clubs_id ";
} 
$q = "select DISTINCT a.* , mdl_ap_action_plans.name as action_plan, mdl_ap_clubs.name as club 
from mdl_ap_club_action_plans a 
LEFT JOIN mdl_ap_action_plans  on a.action_plan_id = mdl_ap_action_plans.id
LEFT JOIN mdl_ap_clubs  on a.club_id = mdl_ap_clubs.id
where 1 = 1 
$and 
order by $sort $dir";
if($debug == 1){echo '$query : ' . $q . ''   ;}
if($download == ''){
//get a page worth of records
$results = $DB->get_records_sql($q,array(),$page*$perpage, $perpage);
}
else{
$results = $DB->get_records_sql($q);
}
if($download == ''){
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
from mdl_ap_club_action_plans a 
LEFT JOIN mdl_ap_action_plans  on a.action_plan_id = mdl_ap_action_plans.id
LEFT JOIN mdl_ap_clubs  on a.club_id = mdl_ap_clubs.id
 where 1 = 1  $and";

if($debug == 1){echo '$query : ' . $q . '<br>'   ;}

$result_count = $DB->get_field_sql($q);
}

require_once('club_action_plan_search_form.php');
$mform = new club_action_plan_search_form(null, array('sort' => $sort,'dir'=>$dir,'perpage'=>$perpage,'search'=>$search,'tab'=>$currenttab, 'debug'=>$debug ,'ap_action_plans_id'=>$ap_action_plans_id,'ap_clubs_id'=>$ap_clubs_id));
$mform->display();
echo '<table width="100%"><tr><td width="50%">';
echo $result_count . ' ' . get_string('club_action_plan_plural','block_action_plans') . " found" . '<br>';
echo '</td><td style="text-align:right;">';
if(has_capability('block/action_plans:edit', $context)){
echo "<a href='index.php?tab=club_action_plan_new'>" . get_string('club_action_plan_new','block_action_plans') . "</a>";
}
echo '</td></tr></table>';



//RESULTS
if (!$results) {
if($download == ''){
echo $OUTPUT->heading(get_string('noresults','block_action_plans',$search));
}
} else {
$table = new html_table();
$table->head = array (
$action_plan,
$club,
$target_date,
'Action'
);
$table->width = "95%";
foreach ($results as $result) {
$view_link = '';//enable if wanted: "<a href='index.php?tab=club_action_plan_view&id=$result->id'>View</a> "; 
$edit_link = "";
$delete_link = "";
if(has_capability('block/action_plans:edit', $context)){
// then we show the edit link
$edit_link = "<a href='index.php?tab=club_action_plan_edit&id=$result->id'>Edit</a> ";
}
if(has_capability('block/action_plans:delete', $context)){
// then we show the delete link
$delete_link = "<a href='index.php?tab=club_action_plan_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
}
$table->data[] = array (
$result->action_plan,
$result->club,
date('d-m-Y',strtotime($result->target_date)),
$view_link . $edit_link . $delete_link
);
}
}
if(!empty($table)){
if ($download != '') {
//export the table to whatever they asked for
block_action_plans_export_data($download,$table,$tab);
}
else {
echo html_writer::table($table);
$pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
$pagingbar->pagevar = 'page';
echo $OUTPUT->render($pagingbar);
block_action_plans_output_download_links($PAGE->url, 'club_action_plan_plural');
}
}

