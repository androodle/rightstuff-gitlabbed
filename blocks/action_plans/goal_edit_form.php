<?php

/** 
 * Action Plans Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     18/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class goal_edit_form extends moodleform {
protected $goal;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.* , mdl_ap_action_plans.name as action_plan 
from mdl_ap_goals a 
LEFT JOIN mdl_ap_action_plans  on a.action_plan_id = mdl_ap_action_plans.id
where a.id = {$_REQUEST['id']} ";
$goal = $DB->get_record_sql($q);
}
else{
$goal = $this->_customdata['$goal']; // this contains the data of this form
}
$tab = 'goal_new'; // from whence we were called
if (!empty($goal->id)) {
$tab = 'goal_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_action_plans'), array('size'=>100));
$mform->setType('name', PARAM_RAW);
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 50 characters', 'maxlength', 255, 'client');

//numeral
$numeral_arr = array(0=>'',1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10);
$mform->addElement('select', 'numeral', get_string('numeral','block_action_plans'), $numeral_arr);

//action_plan_id
$options = $DB->get_records_menu('ap_action_plans',null,'name','id,name'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'action_plan_id', get_string('action_plan','block_action_plans'), $options);
$mform->addRule('action_plan_id', get_string('required'), 'required', null, 'server');
//set values if we are in edit mode
if (!empty($goal->id) && isset($_GET['id'])) {
$mform->setConstant('name', $goal->name);
$mform->setConstant('numeral', $goal->numeral);
$mform->setConstant('action_plan_id', $goal->action_plan_id);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->setType('tab', PARAM_TEXT);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
$mform->setType('id', PARAM_INT);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
$mform->setType('id', PARAM_INT);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
