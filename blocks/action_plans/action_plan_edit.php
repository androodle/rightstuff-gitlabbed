<?php

/** 
 * Action Plans Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     12/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the action_plans
 *  
 **/

global $OUTPUT;
require_capability('block/action_plans:edit', $context);
require_once('action_plan_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.*  
from mdl_ap_action_plans a 
where a.id = $id ";
$action_plan = $DB->get_record_sql($q);
$mform = new action_plan_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$DB->update_record('ap_action_plans',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_action_plans'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('action_plan_edit', 'block_action_plans'));
$mform->display();
}

