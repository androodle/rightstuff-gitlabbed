<?php


/** 
 * Action Plans Block: Settings 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/



defined('MOODLE_INTERNAL') || die(); 



if ($ADMIN->fulltree) {
//example text field (INT). CHANGE MY NAME!
//$settings->add(new admin_setting_configtext('block_action_plans/run_cron_hours', get_string('cron_hours', 'block_action_plans'),
//               get_string('cron_hours_explanation', 'block_action_plans'), '', PARAM_INT));
//edm_from_email
$settings->add(new admin_setting_configtext('block_action_plans/edm_from_email', get_string('edm_from_email', 'block_action_plans'),
               get_string('edm_from_email_explanation', 'block_action_plans'), '', PARAM_EMAIL));

//cron hours
$settings->add(new admin_setting_configtext('block_action_plans/run_cron_hours', get_string('cron_hours', 'block_action_plans'),
               get_string('cron_hours_explanation', 'block_action_plans'), '', PARAM_RAW,50));


}

// End of blocks/action_plans/settings.php
