<?php

/** 
 * Action Plans Block: Delete object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     12/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Delete one of the action_plans
 *  
 **/

require_capability('block/action_plans:edit', $context);
$id = required_param('id', PARAM_INT);
$DB->delete_records('ap_action_plans',array('id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_action_plans'), 'notifysuccess');

