<?php
/** 
 * Action Plans Block: Tabs 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/

// This file to be included so we can assume config.php has already been included.
if (empty($currenttab)) {
error('You cannot call this script in that way');
}
$tabs = array();
$row = array();
//$row[] = new tabobject('club_action_plan_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=club_action_plan_search', get_string('club_action_plan_search','block_action_plans'));
$row[] = new tabobject('club_strategy_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=club_strategy_search', get_string('club_strategy_search','block_action_plans'));
$row[] = new tabobject('club_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=club_search', get_string('club_search','block_action_plans'));
$row[] = new tabobject('action_plan_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=action_plan_search', get_string('action_plan_search','block_action_plans'));
$row[] = new tabobject('goal_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=goal_search', get_string('goal_search','block_action_plans'));
$row[] = new tabobject('objective_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=objective_search', get_string('objective_search','block_action_plans'));
$row[] = new tabobject('strategy_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=strategy_search', get_string('strategy_search','block_action_plans'));
$row[] = new tabobject('edm_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=edm_search', get_string('edm_search','block_action_plans'));
$row[] = new tabobject('edm_log_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=edm_log_search', get_string('edm_log_search','block_action_plans'));
if(is_siteadmin()){
    $row[] = new tabobject('trigger_point_search', $CFG->wwwroot.'/blocks/action_plans/index.php?tab=trigger_point_search', get_string('trigger_point_search','block_action_plans'));
}
$tabs[] = $row;
// Print out the tabs and continue!
print_tabs($tabs, $currenttab);

// End of blocks/action_plans/tabs.php