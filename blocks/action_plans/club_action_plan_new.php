<?php

/** 
 * Action Plans Block: Create object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     02/06/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Create new club_action_plan
 *  
 **/

global $OUTPUT;
require_capability('block/action_plans:edit', $context);
require_once('club_action_plan_edit_form.php');
$mform = new club_action_plan_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
$data->target_date = date('Y-m-d',$data->target_date);
$newid = $DB->insert_record('ap_club_action_plans',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_action_plans'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('club_action_plan_new', 'block_action_plans'));
$mform->display();
}

