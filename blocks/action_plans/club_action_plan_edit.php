<?php

/** 
 * Action Plans Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     02/06/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the club_action_plans
 *  
 **/

global $OUTPUT;
require_capability('block/action_plans:edit', $context);
require_once('club_action_plan_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , mdl_ap_action_plans.name as action_plan, mdl_ap_clubs.name as club 
from mdl_ap_club_action_plans a 
LEFT JOIN mdl_ap_action_plans  on a.action_plan_id = mdl_ap_action_plans.id
LEFT JOIN mdl_ap_clubs  on a.club_id = mdl_ap_clubs.id
where a.id = $id ";
$club_action_plan = $DB->get_record_sql($q);
$mform = new club_action_plan_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$data->target_date = date('Y-m-d',$data->target_date);
$DB->update_record('ap_club_action_plans',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_action_plans'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('club_action_plan_edit', 'block_action_plans'));
$mform->display();
}

