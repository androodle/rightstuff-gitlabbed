<?php

/**
 * Action Plans Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     11/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 * */
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');

class club_strategy_edit_form extends moodleform {

    protected $club_strategy;

    function definition() {
        global $USER, $courseid, $DB, $PAGE;
        $mform = & $this->_form;
        $context = context_system::instance();
        $strategy_id = optional_param('strategy_id', 0, PARAM_INT);
        $club_id = optional_param('club_id', 0, PARAM_INT);
        $id = optional_param('id', 0, PARAM_INT);
        $submitted = optional_param('submitted', 0, PARAM_INT);
        
        if ($strategy_id > 0 && $club_id > 0) {
            $q = "select DISTINCT a.* , a.name as strategy, mdl_ap_clubs.name as club, mdl_files.contextid, mdl_files.component, 
                mdl_files.filearea, mdl_files.filename, mdl_files.itemid, mdl_files.id as fileid, CONCAT(mdl_user.firstname,' ',
                mdl_user.lastname) as user_responsible, user_responsible_id, mdl_ap_club_strategies.id as club_strategy_id, 
                timeframe, resources_required, mdl_ap_club_strategies.status,mdl_ap_club_strategies.tell_us_how
            from mdl_ap_strategies a 
            LEFT JOIN mdl_ap_club_strategies on a.id = mdl_ap_club_strategies.strategy_id and mdl_ap_club_strategies.club_id = $club_id
            LEFT JOIN mdl_ap_objectives o on a.objective_id = o.id
            LEFT JOIN mdl_ap_goals g on o.goal_id = g.id
            LEFT JOIN mdl_ap_clubs on mdl_ap_club_strategies.club_id = mdl_ap_clubs.id 
            LEFT JOIN mdl_files on mdl_ap_club_strategies.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.' 
            LEFT JOIN mdl_user on mdl_ap_club_strategies.user_responsible_id = mdl_user.id ";
            $q .= "where a.id = $strategy_id";
            $club_strategy = $DB->get_record_sql($q);
            if($club_strategy->club_strategy_id != null){
                $id = $club_strategy->club_strategy_id;
            }
        }
        else if ($id > 0){
            $q = "select DISTINCT a.* , a.name as strategy, mdl_ap_clubs.name as club, mdl_files.contextid, mdl_files.component, 
                mdl_files.filearea, mdl_files.filename, mdl_files.itemid, mdl_files.id as fileid, CONCAT(mdl_user.firstname,' ',
                mdl_user.lastname) as user_responsible, user_responsible_id, mdl_ap_club_strategies.id as club_strategy_id, 
                timeframe, resources_required, mdl_ap_club_strategies.status, mdl_ap_club_strategies.tell_us_how, 
                mdl_ap_club_strategies.strategy_id, mdl_ap_club_strategies.club_id
            from mdl_ap_strategies a 
            LEFT JOIN mdl_ap_club_strategies on a.id = mdl_ap_club_strategies.strategy_id 
            LEFT JOIN mdl_ap_objectives o on a.objective_id = o.id
            LEFT JOIN mdl_ap_goals g on o.goal_id = g.id
            LEFT JOIN mdl_ap_clubs on mdl_ap_club_strategies.club_id = mdl_ap_clubs.id 
            LEFT JOIN mdl_files on mdl_ap_club_strategies.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.' 
            LEFT JOIN mdl_user on mdl_ap_club_strategies.user_responsible_id = mdl_user.id ";
            $q .= "where mdl_ap_club_strategies.id = $id";
            $club_strategy = $DB->get_record_sql($q);
            if($club_strategy->strategy_id != null){
                $strategy_id = $club_strategy->strategy_id;
            }
            if($club_strategy->club_id != null){
                $club_id = $club_strategy->club_id;
            }
        }
        else {
            $club_strategy = $this->_customdata['$club_strategy']; // this contains the data of this form
        }

        if (has_capability('block/action_plans:edit', $context)) {
            $sql = "SELECT c.* FROM mdl_ap_clubs c INNER JOIN mdl_ap_club_members cm on c.id = cm.club_id WHERE user_id = $USER->id";
            $user_club = $DB->get_record_sql($sql);
        }
        else {
            $sql = "SELECT c.* FROM mdl_ap_clubs c INNER JOIN mdl_ap_club_members cm on c.id = cm.club_id WHERE user_id = $USER->id";
            $user_club = $DB->get_record_sql($sql);
        }
        if(!isset($user_club->name) and $club_id > 0){
            $user_club = $DB->get_record('ap_clubs',array('id'=>$club_id));
        }
        $tab = 'club_strategy_new'; // from whence we were called
        if (!empty($club_strategy->id)) {
            $tab = 'club_strategy_edit';
        }
        $mform->addElement('html', '<div>');

//strategy_id
        $mform->addElement('static', 'strategy_name', 'Strategy', $club_strategy->strategy);

//club_id
        $mform->addElement('static', 'club_name', 'Club', $user_club->name);
//status
        unset($options);
        $options['notyetstarted'] = 'Not yet started';
        $options['inprogress'] = 'In progress';
        $options['complete'] = 'Complete';
        $select = $mform->addElement('select', 'status', get_string('status', 'block_action_plans'), $options);

//timeframe
        $mform->addElement('date_selector', 'timeframe', get_string('timeframe', 'block_action_plans'));
        $mform->addRule('timeframe', get_string('required'), 'required', null, 'server');

//user_responsible_id
        $sql = "SELECT u.id, concat(firstname, ' ', lastname) "
                . "FROM mdl_user u "
                . "INNER JOIN mdl_ap_club_members cm on u.id = cm.user_id ";
        if ($club_id > 0) {
            $sql.= "WHERE club_id = $club_id";
        }
        $options = $DB->get_records_sql_menu($sql);
        $mform->addElement('select', 'user_responsible_id', get_string('user_responsible', 'block_action_plans'), $options);
        $mform->addRule('user_responsible_id', get_string('required'), 'required', null, 'server');

//resources_required
        $editoroptions = array('maxfiles' => 0, 'maxbytes' => 0, 'trusttext' => false, 'forcehttps' => false);
        $attributes = array('rows'=>5,'cols'=>50);
        $mform->addElement('editor', 'resources_required', get_string('resources_required', 'block_action_plans'),$attributes, $editoroptions);
        $mform->setType('resources_required', PARAM_CLEANHTML);
        
if($club_strategy->status == 'complete'){
        $mform->addElement('header','evidence_header',get_string('evidence','block_action_plans'));
        
//tell_us_how        
        $attributes = array('rows'=>10,'cols'=>50);
        $mform->addElement('editor', 'tell_us_how', get_string('tell_us_how', 'block_action_plans'),$attributes, $editoroptions);
        $mform->setType('tell_us_how', PARAM_CLEANHTML);
        //$mform->disabledIf('tell_us_how', 'status','eq','inprogress|notyetstarted');

        //file_id
        if (empty($club_strategy->fileid)) {
            $mform->addElement('filepicker', 'file_id', get_string('file', 'block_action_plans'));

        }
        else {
            $file_link = new moodle_url("/pluginfile.php/{$club_strategy->contextid}/{$club_strategy->component}/{$club_strategy->filearea}/" . $club_strategy->itemid . '/' . $club_strategy->filename);
            $mform->addElement('static', 'file_id', get_string('file', 'block_action_plans'), '');
            $mform->addElement('button', 'editthisfile', get_string('editthisfile', 'block_action_plans'), array('onclick' => 'location.href="index.php?tab=upload&fileid=' . $club_strategy->fileid . '&object=club_strategy&objectid=' . $club_strategy->club_strategy_id . '&block_name=block_action_plans&table_name=ap_club_strategies&foreign_key_name=file_id"'));
        }
}

//set values if we are in edit mode
        if (!empty($club_strategy->id) && $submitted == 0) {
            if($mform->elementExists('strategy_id')){
                $mform->setConstant('strategy_id', $strategy_id);
            }
            if($mform->elementExists('strategy_id')){
                $mform->setConstant('club_id', $club_id);
            }
            if (isset($file_link)) {
                $mform->setConstant('file_id', html_writer::link($file_link, $club_strategy->filename));
            }
            $mform->setConstant('timeframe', strtotime($club_strategy->timeframe));
            $mform->setConstant('user_responsible_id', $club_strategy->user_responsible_id);
            $resources_required['text'] = $club_strategy->resources_required;
            $resources_required['format'] = 1;
            $mform->setDefault('resources_required', $resources_required);
            if(isset($club_strategy->tell_us_how)){
                $tell_us_how['text'] = $club_strategy->tell_us_how;
                $tell_us_how['format'] = 1;
                $mform->setDefault('tell_us_how', $tell_us_how);
            }
            $mform->setConstant('status', $club_strategy->status);
        }
//hiddens
        $mform->addElement('hidden', 'tab', $tab);
        $mform->setType('tab', PARAM_TEXT);
        if ($id > 0) {
            $mform->addElement('hidden', 'id', $id);
            $mform->setType('id', PARAM_INT);
        }
        $mform->addElement('hidden', 'club_id', $club_id);
        $mform->setType('club_id', PARAM_INT);
        $mform->addElement('hidden', 'strategy_id', $strategy_id);
        $mform->setType('strategy_id', PARAM_INT);
        $mform->addElement('hidden', 'submitted', 1);
        $mform->setType('submitted', PARAM_INT);
        
        $this->add_action_buttons(false);
        $mform->addElement('html', '</div>');
    }

}
