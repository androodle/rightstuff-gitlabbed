<?php
/** 
 * Action Plans Block: Language Pack 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/
$string['pluginname'] = 'Action Plans';
$string['plugintitle'] = 'Action Plans';
//spares
$string['admin'] = 'Action Plan';
$string['user'] = 'User';
// signup form strings
$string['createsupplementaryuser'] = '2. Create a second user for your canteen (optional)';
$string['createyourownuser'] = '1. Create your own user account';
$string['createsupplementaryuserandpass'] = 'Choose their username and password';

$string['course'] = 'Course';
$string['name'] = 'Name';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';
//common items
$string['datasubmitted'] = 'The data has been submitted';
$string['itemdeleted'] = 'The item has been deleted';
$string['noresults'] = 'There were no results from your search';
$string['block_action_plans:edit'] = 'Edit objects within the Action Plans block';
$string['block_action_plans:delete'] = 'Delete objects within the Action Plans block';

//action_plan items
$string['action_plan'] = 'Action Plan';
$string['action_plan_search'] = 'Action Plans';
$string['action_plan_plural'] = 'Action Plans';
$string['action_plan_new'] = 'New Action Plan';
$string['action_plan_edit'] = 'Edit Action Plan';
$string['action_plan_search_instructions'] = 'Search by Name';
$string['ap_action_plan'] = 'Action Plan';
$string['plan'] = 'Plan';
$string['club_name'] = 'Club name';
$string['action_plan_report'] = 'Finish With the Right Stuff Action Plan for {$a}';
$string['action_plan_heading'] = 'Action plan heading';

//edm items
$string['edm'] = 'EDM';
$string['body'] = 'Body';
$string['enabled'] = 'Enabled';
$string['trigger_point'] = 'Trigger Point';
$string['days_after'] = 'Days after';
$string['body_help'] = 'You can use certain placeholders in the body text, with the intention that at run time these will be replaced with entries from the database. '
        . ' These are: user, username, module and admin. They must be enclosed with square brackets in this way: [user] - ie no spaces, and lowercase. '
        . '<ul><li>[user] will be replaced by the user\'s full name. <li>[username] will be replaced by their username. '
        . '<li>[email] will be replaced by their email.<li>[clubname] will be replaced by their clubname.'
        . '<li>[uname_rep_1] will be replaced by the username of the first rep of the user\'s club (which may be them).'
        . '<li>[uname_rep_2] will be replaced by the username of the second rep of the user\'s club (which may be them).'
        . '<li>[module] will be replaced with the full name of the module. <li>[admin] will insert the name of the admin user.</ul>';
$string['edm_search'] = 'EDMs';
$string['edm_plural'] = 'EDMs';
$string['edm_new'] = 'New EDM';
$string['edm_edit'] = 'Edit EDM';
$string['edm_search_instructions'] = 'Search by Name or Body';
//trigger_point items
$string['trigger_point_search'] = 'Trigger Points';
$string['trigger_point_plural'] = 'Trigger Points';
$string['trigger_point_new'] = 'New Trigger Point';
$string['trigger_point_edit'] = 'Edit Trigger Point';
$string['trigger_point_search_instructions'] = 'Search by Name';
$string['trigger_point_id'] = 'Trigger Point'; 
$string['ap_trigger_points'] = 'Trigger Point';

//goal items
$string['goal'] = 'Goal';
$string['goal_search'] = 'Goals';
$string['goal_plural'] = 'Goals';
$string['goal_new'] = 'New Goal';
$string['goal_edit'] = 'Edit Goal';
$string['goal_id'] = 'Id Goal';
$string['goal_search_instructions'] = 'Search by Name';
$string['ap_goal'] = 'Goal';
$string['numeral'] = 'Order';

//objective items
$string['objective'] = 'Objective';
$string['objective_search'] = 'Objectives';
$string['objective_plural'] = 'Objectives';
$string['objective_new'] = 'New Objective';
$string['objective_edit'] = 'Edit Objective';
$string['objective_id'] = 'Id Objective';
$string['objective_search_instructions'] = 'Search by Name';
$string['ap_objective'] = 'Objective';
//strategy items
//$string['strategy'] = 'Strategy';
$string['strategy'] = 'Health action';
$string['timeframe'] = 'Timeframe';
$string['user_responsible'] = 'User Responsible';
$string['resources_required'] = 'Resources Required';
$string['status'] = 'Status';
$string['strategy_search'] = 'Health actions';
$string['strategy_plural'] = 'Health actions';
$string['strategy_new'] = 'New Health action';
$string['strategy_edit'] = 'Edit Health action';
$string['strategy_id'] = 'Id Strategy';
$string['strategy_search_instructions'] = 'Search by Status';
$string['strategy_search_instructions'] = 'Search by Health action, Resources Required or  Status';
//club_strategy items
$string['club_strategy'] = 'Club Strategy';
$string['club'] = 'Club';
$string['file'] = 'Evidence file';
$string['club_strategy_search'] = 'Club Health actions';
$string['club_strategy_plural'] = 'Club Health actions';
$string['club_strategy_new'] = 'New Club Health action';
$string['club_strategy_edit'] = 'Edit Club Health action';
$string['club_strategy_id'] = 'Id Club Strategy';
$string['club_strategy_search_instructions'] = 'Search by Resources Required or  Status';
$string['editthisfile'] = 'Edit this file';
$string['upload'] = 'Upload';
$string['tell_us_how'] = 'Tell us how you did it';
$string['evidence'] = "Evidence";

$string['instructions_step1'] = "Step 1: Select the healthy actions that you would like to implement at your club canteen/association by clicking ‘Choose’ on the below list of healthy actions.";
$string['instructions_step2'] = "Step 2: Keep the status of the Action Plan up-to-date, by clicking onto the ‘Update’ button for the chosen healthy action";

//club items
$string['club_search'] = 'Clubs';
$string['club_plural'] = 'Clubs';
$string['club_new'] = 'New Club';
$string['club_edit'] = 'Edit Club';
$string['club_id'] = 'Id Club';
$string['club_search_instructions'] = 'Search by Name';

//statuses
$string['notyetstarted'] = "Not yet started";
$string['inprogress'] = "In progress";
$string['complete'] = "Complete";
$string['show_available_only'] = "Show available health actions only";

//edm_log items
$string['edm_log'] = 'EDM Log';
$string['user'] = 'User';
$string['time_sent'] = 'Time Sent';
$string['edm_log_search'] = 'EDM Logs';
$string['edm_log_plural'] = 'EDM Logs';
$string['edm_log_new'] = 'New EDM Log';
$string['edm_log_edit'] = 'Edit EDM Log';
$string['edm_log_id'] = 'Id EDM Log';
$string['edm_log_search_instructions'] = 'Search by Name';
$string['startdate'] = 'Start date';
$string['enddate'] = 'End date';
$string['subject'] = 'Subject';
$string['edm_send_result'] = 'Result';
$string['edm_from_email'] = 'EDM "from" email address';
$string['edm_from_email_explanation'] = 'EDMs will come from this email address';

//club_action_plan items
$string['club_action_plan'] = 'Club Action Plan';
$string['club_action_plan_search'] = 'Club Action Plans';
$string['club_action_plan_plural'] = 'Club Action Plans';
$string['club_action_plan_new'] = 'New Club Action Plan';
$string['club_action_plan_edit'] = 'Edit Club Action Plan';
$string['target_date'] = 'Target date';
$string['club_action_plan_search_instructions'] = 'Search by club name';

$string['search'] = 'Search';

//cron strings
$string['cron_hours'] = 'Cron hours';
$string['cron_hours_explanation'] = 'The hour at which the cron should execute in 24 hour format(0-23). Can be multiple: if so use semi-colon delimited list. Note that if this is not set then the period expiry mechanism will not work.';
