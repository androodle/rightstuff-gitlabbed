<?php

/** 
 * Action Plans Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     11/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the strategies
 *  
 **/

global $OUTPUT;
require_capability('block/action_plans:edit', $context);
require_once('strategy_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , mdl_ap_objectives.name as objective 
from mdl_ap_strategies a 
LEFT JOIN mdl_ap_objectives  on a.objective_id = mdl_ap_objectives.id
where a.id = $id ";
$strategy = $DB->get_record_sql($q);
$mform = new strategy_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$DB->update_record('ap_strategies',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_action_plans'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('strategy_edit', 'block_action_plans'));
$mform->display();
}

