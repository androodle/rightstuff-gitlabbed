<?php

/** 
 * Action Plans Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     18/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class objective_edit_form extends moodleform {
protected $objective;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.* , mdl_ap_goals.name as goal, mdl_course.fullname as course 
from mdl_ap_objectives a 
LEFT JOIN mdl_ap_goals  on a.goal_id = mdl_ap_goals.id
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where a.id = {$_REQUEST['id']} ";
$objective = $DB->get_record_sql($q);
}
else{
$objective = $this->_customdata['$objective']; // this contains the data of this form
}
$tab = 'objective_new'; // from whence we were called
if (!empty($objective->id)) {
$tab = 'objective_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_action_plans'), array('size'=>100));
$mform->setType('name', PARAM_RAW);
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 50 characters', 'maxlength', 255, 'client');

//numeral
$numeral_arr = array(0=>'',1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10);
$mform->addElement('select', 'numeral', get_string('numeral','block_action_plans'), $numeral_arr);

//goal_id
$options = $DB->get_records_menu('ap_goals',null,'name','id,name'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'goal_id', get_string('goal','block_action_plans'), $options);
$mform->addRule('goal_id', get_string('required'), 'required', null, 'server');

//course_id
$options = $DB->get_records_menu('course',null,'fullname','id,fullname'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'course_id', get_string('course','block_action_plans'), $options);
$mform->addRule('course_id', get_string('required'), 'required', null, 'server');
//set values if we are in edit mode
if (!empty($objective->id) && isset($_GET['id'])) {
$mform->setConstant('name', $objective->name);
$mform->setConstant('numeral', $objective->numeral);
$mform->setConstant('goal_id', $objective->goal_id);
$mform->setConstant('course_id', $objective->course_id);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->setType('tab', PARAM_TEXT);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
$mform->setType('id', PARAM_INT);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
$mform->setType('id', PARAM_INT);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
