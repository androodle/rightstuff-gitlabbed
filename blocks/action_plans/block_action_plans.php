<?php
/** 
 * Action Plans Block: Class 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/
require_once($CFG->dirroot.'/blocks/action_plans/lib.php');

class block_action_plans extends block_base {


	function init(){
		$this->title = get_string('plugintitle','block_action_plans');
                $this->cron = 1;
	} //end function init


	function get_content(){
		global $CFG;
		if ($this->content !== null) {
			return $this->content;
		}
		$this->content = new stdClass;
		$this->content->text = '<a href="'.$CFG->wwwroot.'/blocks/action_plans/index.php">'.get_string('admin','block_action_plans').'</a><br>';
		$context = context_system::instance();
		if(has_capability('moodle/site:config', $context)){
			$this->content->text .= '<a href="'.$CFG->wwwroot.'/admin/settings.php?section=blocksettingaction_plans">'.get_string('settings').'</a><br>';
		}
		return $this->content;


	} //end function get_content

        function cron(){
            mtrace('starting action_plans cron... ');
            block_action_plans_cron();
	} //end function cron
        
	function has_config(){
		return true;
	} //end function has_config


} //end class block_action_plans


// End of blocks/action_plans/block_action_plans.php
