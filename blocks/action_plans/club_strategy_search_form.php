<?php

/** 
 * Action Plans Block: Search form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     11/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides search form for the object.
 * This is used by search page
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class club_strategy_search_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$mform->addElement('html','<div>');
//search controls
$mform->addElement('header', 'searchhdr', get_string('search', 'block_action_plans'));
$mform->setExpanded('searchhdr', false);
$mform->addElement('text','search',get_string('club_strategy_search_instructions', 'block_action_plans'));
$mform->setType('search', PARAM_RAW);
//ap_strategies_id
$dboptions = $DB->get_records_menu('ap_strategies',array(),'name','id,left(name,100)');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'ap_strategies_id', get_string('strategy','block_action_plans'), $options);

if(has_capability('block/action_plans:edit', $context)){
    //ap_clubs_id
    $dboptions = $DB->get_records_menu('ap_clubs',array(),'name','id,name');
    unset($options);
    $options[0] = 'Any';
    foreach($dboptions as $key=>$value){
    $options[$key] = $value;
    }
    $select = $mform->addElement('select', 'ap_clubs_id', get_string('club','block_action_plans'), $options);
}
//user_id
$sql = "SELECT u.id,concat(firstname,' ',lastname) as fullname FROM mdl_user u inner join mdl_ap_club_members cm on u.id = cm.user_id WHERE u.deleted = 0 ";
if(!is_siteadmin() && !empty($user_club)){
    $sql .= " and cm.club_id = $user_club->id";
}
$dboptions = $DB->get_records_sql_menu($sql);
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'user_id', get_string('user','block_action_plans'), $options);
//status
$sql = 'SELECT DISTINCT status FROM mdl_ap_club_strategies WHERE status != \'\' ORDER BY status';
$dboptions = $DB->get_records_sql($sql);
unset($options);
$options[0] = 'Any';
foreach($dboptions as $dboption){
$options[$dboption->status] = get_string($dboption->status,'block_action_plans');
}
$select = $mform->addElement('select', 'filter_status', get_string('status','block_action_plans'), $options);

//show_available_only
$mform->addElement('selectyesno', 'show_available_only', get_string('show_available_only', 'block_action_plans'));

//set some values if they have been passed in
foreach($this->_customdata as $custom_key=>$custom_value){
    if(isset($mform->_elementIndex[$custom_key])){
        $mform->setConstant($custom_key,$custom_value);
    }
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->setType('tab', PARAM_TEXT);
$mform->addElement('hidden','sort',$sort);
$mform->setType('sort', PARAM_TEXT);
$mform->addElement('hidden','dir',$dir);
$mform->setType('dir', PARAM_TEXT);
$mform->addElement('hidden','perpage',$perpage);
$mform->setType('perpage', PARAM_INT);

//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
