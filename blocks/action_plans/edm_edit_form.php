<?php

/** 
 * Action Plans Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     21/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class edm_edit_form extends moodleform {
protected $edm;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.* , mdl_course.fullname as course, mdl_ap_trigger_points.name as trigger_point 
from mdl_ap_edms a 
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
LEFT JOIN mdl_ap_trigger_points  on a.trigger_point_id = mdl_ap_trigger_points.id
where a.id = {$_REQUEST['id']} ";
$edm = $DB->get_record_sql($q);
}
else{
$edm = $this->_customdata['$edm']; // this contains the data of this form
}
$tab = 'edm_new'; // from whence we were called
if (!empty($edm->id)) {
$tab = 'edm_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_action_plans'), array('size'=>50));
$mform->setType('name', PARAM_RAW);
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 50 characters', 'maxlength', 50, 'client');

//subject
$mform->addElement('text', 'subject', get_string('subject','block_action_plans'), array('size'=>45));
$mform->setType('subject', PARAM_RAW);
$mform->addRule('subject', 'Maximum 45 characters', 'maxlength', 45, 'client');

//body
$editoroptions = array('maxfiles' => 0, 'maxbytes' => 0, 'trusttext' => false, 'forcehttps' => false);
$mform->addElement('editor', 'body', get_string('body','block_action_plans'), null, $editoroptions);
$mform->setType('body', PARAM_CLEANHTML);
$mform->addRule('body', get_string('required'), 'required', null, 'server');
$mform->addHelpButton('body', 'body', 'block_action_plans');

//enabled
$mform->addElement('selectyesno', 'enabled', get_string('enabled','block_action_plans'));
$mform->addRule('enabled', get_string('required'), 'required', null, 'server');

//course_id
$options = $DB->get_records_menu('course',null,'fullname','id,fullname'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'course_id', get_string('course','block_action_plans'), $options);
$mform->addRule('course_id', get_string('required'), 'required', null, 'server');

//trigger_point_id
$options = $DB->get_records_menu('ap_trigger_points',null,'name','id,name'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'trigger_point_id', get_string('trigger_point_id','block_action_plans'), $options);
$mform->addRule('trigger_point_id', get_string('required'), 'required', null, 'server');

//days_after
$mform->addElement('text', 'days_after', get_string('days_after','block_action_plans'), array('size'=>50));
$mform->setType('days_after', PARAM_RAW);
$mform->addRule('days_after', 'Maximum 50 characters', 'maxlength', 50, 'client');


//set values if we are in edit mode
if (!empty($edm->id) && isset($_GET['id'])) {
$mform->setConstant('name', $edm->name);
$body['text'] = $edm->body;
$body['format'] = 1;
$mform->setDefault('body', $body);
$mform->setConstant('enabled', $edm->enabled);
$mform->setConstant('course_id', $edm->course_id);
$mform->setConstant('trigger_point_id', $edm->trigger_point_id);
$mform->setConstant('days_after', $edm->days_after);
$mform->setConstant('subject', $edm->subject);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->setType('tab', PARAM_TEXT);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
$mform->setType('id', PARAM_INT);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
$mform->setType('id', PARAM_INT);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
