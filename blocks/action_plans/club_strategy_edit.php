<?php

/**
 * Action Plans Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     11/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the club_strategies
 *  
 * */
global $OUTPUT;
//require_capability('block/action_plans:edit', $context);
require_once('club_strategy_edit_form.php');
$strategy_id = optional_param('strategy_id', 0, PARAM_INT);
$club_id = optional_param('club_id', 0, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);

$q = "select DISTINCT a.* , mdl_ap_strategies.name as strategy, mdl_ap_clubs.name as club, mdl_files.contextid, 
    mdl_files.component, mdl_files.filearea, mdl_files.filename, mdl_files.itemid, mdl_files.id as fileid, 
    CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user_responsible 
from mdl_ap_club_strategies a 
LEFT JOIN mdl_ap_strategies  on a.strategy_id = mdl_ap_strategies.id
LEFT JOIN mdl_ap_clubs  on a.club_id = mdl_ap_clubs.id
LEFT JOIN mdl_files  on a.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.'
LEFT JOIN mdl_user  on a.user_responsible_id = mdl_user.id ";
if($strategy_id > 0 && $club_id > 0){
    $q .= "where a.strategy_id = $strategy_id and a.club_id = $club_id";
}
else if($id > 0){
    $q .= "where mdl_ap_strategies.id = $id";
}
$club_strategy = $DB->get_record_sql($q);
$mform = new club_strategy_edit_form();
if ($data = $mform->get_data()) {
    if ($id > 0) {
        $data->id = $id;
    }
    if(isset($data->file_id) && !is_number($data->file_id)){
        // fix the file if need be
        block_action_plans_fix_phantom_file_id($data);
    }
    
    $data->timeframe = date('Y-m-d', $data->timeframe);
    $data->resources_required = format_text($data->resources_required['text'], $data->resources_required['format']);
    if(isset($data->tell_us_how)){
        $data->tell_us_how = format_text($data->tell_us_how['text'], $data->tell_us_how['format']);
    }
    if ($id > 0) {
        $data->modified_by = $USER->id;
        $data->date_modified = date('Y-m-d H:i:s');
        $DB->update_record('ap_club_strategies', $data);
    }
    else {
        $data->created_by = $USER->id;
        $data->date_created = date('Y-m-d H:i:s');
        $DB->insert_record('ap_club_strategies', $data);
    }
    if($data->status == 'complete' and !isset($data->tell_us_how)){
        //represent the form
        echo $OUTPUT->heading(get_string('club_strategy_edit', 'block_action_plans'));
        //have to recreate it to get the saved data
        $mform = new club_strategy_edit_form();
        $mform->display();
    }
    else{
        redirect($CFG->wwwroot . "/blocks/action_plans/index.php");
    }
}
else {
    echo $OUTPUT->heading(get_string('club_strategy_edit', 'block_action_plans'));
    $mform->display();
}

