<?php

/** 
 * Action Plans Block: Create object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     11/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Create new club_strategy
 *  
 **/

global $OUTPUT;
require_capability('block/action_plans:edit', $context);
require_once('club_strategy_edit_form.php');
$mform = new club_strategy_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
$data->timeframe = date('Y-m-d',$data->timeframe);
$data->resources_required = format_text($data->resources_required['text'], $data->resources_required['format']);
//fix file entries
$sql = "UPDATE mdl_files
SET filearea = 'content',component='block_action_plans'
WHERE itemid = '$data->file_id'";
$DB->execute($sql);
$newid = $DB->insert_record('ap_club_strategies',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_action_plans'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('club_strategy_new', 'block_action_plans'));
$mform->display();
}

