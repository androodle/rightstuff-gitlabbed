<?php

/** 
 * Action Plans Block: Create object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     18/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Create new goal
 *  
 **/

global $OUTPUT;
require_capability('block/action_plans:edit', $context);
require_once('goal_edit_form.php');
$mform = new goal_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
$newid = $DB->insert_record('ap_goals',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_action_plans'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('goal_new', 'block_action_plans'));
$mform->display();
}

