<?php

/** 
 * Action Plans Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     21/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the edms
 *  
 **/

global $OUTPUT;
require_capability('block/action_plans:edit', $context);
require_once('edm_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , mdl_course.fullname as course, mdl_ap_trigger_points.name as trigger_point 
from mdl_ap_edms a 
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
LEFT JOIN mdl_ap_trigger_points  on a.trigger_point_id = mdl_ap_trigger_points.id
where a.id = $id ";
$edm = $DB->get_record_sql($q);
$mform = new edm_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$data->body = format_text($data->body['text'], $data->body['format']);
$DB->update_record('ap_edms',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_action_plans'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('edm_edit', 'block_action_plans'));
$mform->display();
}

