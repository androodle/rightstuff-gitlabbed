<?php


/** 
 * Action Plans Block: Lib 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/

/**************************** STANDARD BLOCK BUSTER FUNCTIONS *************************************************/

function block_action_plans_cron($source = 'cron') {
    global $CFG, $DB;
    $config = get_config('block_action_plans');
    $hour = date('G');
    mtrace("the hour: $hour");
    mtrace('the  run cron hours: ');
    var_dump($config->run_cron_hours);
    mtrace('</pre>');
    $run_cron = false;
    if($source == 'test cron'){
        $run_cron = true;
    }
    else{
        if (isset($config->run_cron_hours) && in_array($hour, explode(';', $config->run_cron_hours))) {
            $run_cron = true;
        }
    }
    
    if ($run_cron) {
        mtrace("\n\n");
        mtrace("Running cron for action_plans" . "\n");
        $time = date("d-m-Y h-i-s", time());
        mtrace($time . '\n');
        mtrace('before send out edms:\n');
        send_out_edms();
        enrol_completed_users_in_next_module();
        $time = date("d-m-Y h-i-s", time());
        mtrace('Finish time' . $time . '\n');
    } else {
        mtrace("\n\n");
        mtrace("not one of the run cron hours for action_plans");
    }
}

function block_action_plans_save_upload_over_existing($form_data, $block_name, $table_name, $foreign_key_name) {
    global $DB;
    $data = $form_data->get_data();
    
    //change the reference to the itemid in the block table
    $sql = "update mdl_$table_name set $foreign_key_name = " . $data->itemid . " where id = " . $data->objectid;
    $DB->execute($sql);
    if(isset($data->itemid) and $data->itemid > 0){
        //change the component and filearea of the file entry
        $sql = "UPDATE mdl_files
            SET filearea = 'content',component='$block_name'
            WHERE itemid = '$data->itemid'";
        $DB->execute($sql);
    }
}

function block_action_plans_pluginfile($course, $birecord, $context, $filearea, $args, $forcedownload) {
    global $DB;
    $sql = 'select * from mdl_files 
            where itemid = ' . $args[0] . ' 
                and filename = \'' . $args[1] . '\'
                and contextid = ' . $context->id . ' 
                and filearea = \'' . $filearea . '\'';
    $file = $DB->get_record_sql($sql);
    if (!$file) {
        send_file_not_found();
    } else {
        $fs = get_file_storage();
        $stored_file = $fs->get_file_by_hash($file->pathnamehash);
    }
    $forcedownload = true;

    session_get_instance()->write_close();
    send_stored_file($stored_file, 60 * 60, 0, $forcedownload);
}
function block_action_plans_fix_phantom_file_id($data) {
    global $DB;
    if(isset($data->file_id) and $data->file_id > 0){
        //have we got a phantom file id?
        $sql = "SELECT * FROM mdl_files
        WHERE itemid = '$data->file_id'";
        $file_exists = $DB->get_records_sql($sql);
        if ($file_exists) {
            //fix file entries
            block_action_plans_fix_file($data->file_id);
        } else {
            //get rid of the phantom out of the subject table
            $sql = "UPDATE mdl_ap_club_strategies
            SET file_id = NULL
            where id = '$data->id'";
            $DB->execute($sql);
        }
    }
}
function block_action_plans_fix_file($item_id) {
    global $DB;
    if($item_id > 0){
    $sql = "UPDATE mdl_files
            SET filearea = 'content',component='block_action_plans'
            WHERE itemid = '$item_id'";
            $DB->execute($sql);
    }
}
function block_action_plans_export_data($download_format, $table, $filename,$report_name_lang='',$table2=null,$club_name='') {
    $stripped_table = block_action_plans_strip_tags_from_table($table);
    $function = "block_action_plans_export_data_" . $download_format;
    if($table2){
        $stripped_table2 = block_action_plans_strip_tags_from_table($table2);
        $function($stripped_table, $filename,$report_name_lang,$stripped_table2,$club_name);
    }
    else{
        $function($stripped_table, $filename,$report_name_lang,$table2,$club_name);
    }
}

function block_action_plans_export_data_excel($table, $filename,$report_name_lang,$table2=null,$club_name='') {
    global $CFG;

    require_once("$CFG->libdir/excellib.class.php");

    $filename = clean_filename($filename) . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($table->head as $cell) {
        $worksheet[0]->write(0, $col, $cell);
        $col++;
    }

    $row = 1;
    foreach ($table->data as $row_data) {
        $col = 0;
        foreach ($row_data as $cell) {
            $worksheet[0]->write($row, $col, $cell);
            $col++;
        }
        $row++;
    }
    if($table2){
        //add empty row
        $worksheet[0]->write($row, 0, 0);
        $row++;
        $col = 0;
        foreach ($table2->head as $cell) {
            $worksheet[0]->write($row, $col, $cell);
            $col++;
        }
        $row++;
        foreach ($table2->data as $row_data) {
            $col = 0;
            foreach ($row_data as $cell) {
                $worksheet[0]->write($row, $col, $cell);
                $col++;
            }
            $row++;
        }
    }

    $workbook->close();
    die;
}

function block_action_plans_export_data_csv($table, $filename,$report_name_lang,$table2=null,$club_name='') {
    global $CFG;

    require_once($CFG->libdir . '/csvlib.class.php');

    $filename = clean_filename($filename) . '.csv';

    $csvexport = new csv_export_writer();
    $csvexport->set_filename($filename);
    $csvexport->add_data($table->head);

    foreach ($table->data as $row_data) {
        $csvexport->add_data($row_data);
    }
    if($table2){
        $empty_row = array();
        $csvexport->add_data($empty_row);
        $csvexport->add_data($table2->head);

        foreach ($table2->data as $row_data) {
            $csvexport->add_data($row_data);
        }
    }
    $csvexport->download_file();
    die;
}

function block_action_plans_strip_tags_from_table($table) {
    //strip any html out of the table head and rows
    $stripped_table = new stdClass();
    foreach ($table->head as $table_head_cell) {
        $stripped_table->head[] = strip_tags($table_head_cell);
    }
    foreach ($table->data as $row_data) {
        $stripped_row_data = array();
        foreach ($row_data as $cell) {
            $stripped_row_data[] = strip_tags($cell);
        }
        $stripped_table->data[] = $stripped_row_data;
    }
    return $stripped_table;
}

function block_action_plans_export_data_pdf_portrait($table, $filename, $report_name_lang_entry,$table2=null,$club_name='') {
    block_action_plans_export_data_pdf($table, $filename, $report_name_lang_entry, true,$table2,$club_name);
}

function block_action_plans_export_data_pdf_landscape($table, $filename, $report_name_lang_entry,$table2=null,$club_name='') {
    block_action_plans_export_data_pdf($table, $filename, $report_name_lang_entry, false,$table2,$club_name);
}

function block_action_plans_export_data_pdf($table, $filename, $report_name_lang_entry, $portrait = true,$table2=null,$club_name='') {
    global $CFG;
    //set up some values - these might go to settings page at some stage
    $margin_footer = 10;
    $margin_bottom = 20;
    $font_size_title = 20;
    $font_size_record = 14;
    $font_size_data = 10;
    $memory_limit = 1024;

    require_once $CFG->libdir . '/pdflib.php';

    // Increasing the execution time to no limit.
    set_time_limit(0);
    $filename = clean_filename($filename . '.pdf');

    // Table.
    $html = '';
    $numfields = count($table->head);

    // Layout options.
    if ($portrait) {
        $pdf = new PDF('P', 'mm', 'A4', true, 'UTF-8');
    } else {
        $pdf = new PDF('L', 'mm', 'A4', true, 'UTF-8');
    }

    $pdf->setTitle($filename);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    $pdf->SetFooterMargin($margin_footer);
    $pdf->SetAutoPageBreak(true, $margin_bottom);
    $pdf->AddPage();

    // Get current language to set the font properly.
    $language = current_language();
    if (in_array($language, array('zh_cn', 'ja'))) {
        $font = 'droidsansfallback';
    } else if ($language == 'th') {
        $font = 'cordiaupc';
    } else {
        $font = 'freesans';
    }
    // Check if language is RTL.
    if (right_to_left()) {
        $pdf->setRTL(true);
    }
    if($report_name_lang_entry != ''){
        $report_header = get_string($report_name_lang_entry, 'block_action_plans',$club_name);
        $pdf->SetFont($font, 'B', $font_size_title);
        $pdf->Write(0, format_string($report_header), '', 0, 'L', true, 0, false, false, 0);    
    }
    
    $count = count($table->data);
    $resultstr = $count == 1 ? 'record' : 'records';
    $recordscount = $count . ' ' .$resultstr;
    $pdf->SetFont($font, 'B', $font_size_record);
    if(!$table2){
        $pdf->Write(0, $recordscount, '', 0, 'L', true, 0, false, false, 0);
    }
    $pdf->SetFont($font, '', $font_size_data);

    if (isset($restrictions) && is_array($restrictions) && count($restrictions) > 0) {
        $pdf->Write(0, get_string('reportcontents', 'totara_reportbuilder'), '', 0, 'L', true, 0, false, false, 0);
        foreach ($restrictions as $restriction) {
            $pdf->Write(0, $restriction, '', 0, 'L', true, 0, false, false, 0);
        }
    }

    // Add report caching data.
    if (isset($cache)) {
        $usertz = totara_get_clean_timezone();
        $a = userdate($cache['lastreport'], '', $usertz);
        $lastcache = get_string('report:cachelast', 'totara_reportbuilder', $a);
        $pdf->Write(0, $lastcache, '', 0, 'L', true, 0, false, false, 0);
    }
    //make a space between the header and the table
    $html .= '<p>&nbsp;</p>';
    $html .= '<table border="1" cellpadding="2" cellspacing="0">
                        <thead>
                            <tr style="background-color: #CCC;">';
    foreach ($table->head as $field) {
        $html .= '<th>' . $field . '</th>';
    }
    $html .= '</tr></thead><tbody>';

    foreach ($table->data as $record_data) {
        $html .= '<tr>';
        for ($j = 0; $j < $numfields; $j++) {
            if (isset($record_data[$j])) {
                $cellcontent = html_entity_decode($record_data[$j], ENT_COMPAT, 'UTF-8');
            } else {
                $cellcontent = '';
            }
            $html .= '<td>' . $cellcontent . '</td>';
        }
        $html .= '</tr>';

        // Check memory limit.
        $mramuse = ceil(((memory_get_usage(true) / 1024) / 1024));
        if ($memory_limit <= $mramuse) {
            // Releasing resources.
            $records->close();
            // Notice message.
            print_error('exportpdf_mramlimitexceeded', 'totara_reportbuilder', '', $memory_limit);
        }
    }
    $html .= '</tbody></table>';
    
    if($table2){
        //reset the numfields
        $numfields = count($table2->head);
        //make a space between the tables
        $html .= '<p>&nbsp;</p>';
        //make some more html for table2
        $html .= '<table border="1" cellpadding="2" cellspacing="0">
                    <thead>
                        <tr style="background-color: #CCC;">';
        foreach ($table2->head as $field) {
            $html .= '<th>' . $field . '</th>';
        }
        $html .= '</tr></thead><tbody>';

        foreach ($table2->data as $record_data) {
            $html .= '<tr>';
            for ($j = 0; $j < $numfields; $j++) {
                if (isset($record_data[$j])) {
                    $cellcontent = html_entity_decode($record_data[$j], ENT_COMPAT, 'UTF-8');
                } else {
                    $cellcontent = '';
                }
                $html .= '<td>' . $cellcontent . '</td>';
            }
            $html .= '</tr>';

            // Check memory limit.
            $mramuse = ceil(((memory_get_usage(true) / 1024) / 1024));
            if ($memory_limit <= $mramuse) {
                // Releasing resources.
                $records->close();
                // Notice message.
                print_error('exportpdf_mramlimitexceeded', 'totara_reportbuilder', '', $memory_limit);
            }
        }
        $html .= '</tbody></table>';
    }

    // Closing the pdf.
    $pdf->WriteHTML($html, true, false, false, false, '');

    // Returning the complete pdf.
    //if (!$file) {
        $pdf->Output($filename, 'D');
//    } else {
//        $pdf->Output($file, 'F');
//    }
}
function block_action_plans_output_download_links($moodle_url, $report_name_lang_entry='') {
    $url = $moodle_url->out(false);
    $report_name_param = "&report_name_lang=$report_name_lang_entry";
    echo 'Download: ';
    $download_report_url = $url . "&download=csv";
    echo html_writer::link($download_report_url, 'CSV');
    echo "&nbsp;";
    $download_report_url = $url . "&download=excel";
    echo html_writer::link($download_report_url, 'Excel');
    echo "&nbsp;";
    $download_report_url = $url . "&download=pdf_portrait" . $report_name_param;
    echo html_writer::link($download_report_url, 'PDF Portrait');
    echo "&nbsp;";
    $download_report_url = $url . "&download=pdf_landscape" . $report_name_param;
    echo html_writer::link($download_report_url, 'PDF Landscape');
}

/**************************** END OF STANDARD BLOCK BUSTER FUNCTIONS *************************************************/

function ensure_club_exists($club_name) {
    global $DB,$USER;
    $result = $DB->get_record('ap_clubs',array('name'=>$club_name));
    if($result){
        return $result->id;
    }
    else{
        $club = new stdClass();
        $club->name = $club_name;
        $club->created_by = $USER->id;
        $club->date_created = date('Y-m-d h:i:s');
        $id = $DB->insert_record('ap_clubs',$club);
        return $id;
    }
}
function ensure_user_is_club_member($club_id,$user_id) {
    global $DB,$USER;
    $result = $DB->get_record('ap_club_members',array('club_id'=>$club_id,'user_id'=>$user_id));
    if($result){
        return $result->id;
    }
    else{
        $club_member = new stdClass();
        $club_member->club_id = $club_id;
        $club_member->user_id = $user_id;
        $club_member->created_by = $USER->id;
        $club_member->date_created = date('Y-m-d h:i:s');
        $id = $DB->insert_record('ap_club_members',$club_member);
        return $id;
    }
}

function send_out_edms() {
    global $DB;
    //any edms to send out?
    
    //has anyone completed a Module, that we haven't sent to already?
    $edms = $DB->get_records('ap_edms',array('name'=>'Module completion EDM','enabled'=>1));
    
    foreach($edms as $edm){
        $sql = "select cc.* from mdl_course_completions cc
            left join mdl_ap_edm_log el on cc.userid = el.user_id and cc.course = el.course_id and el.edm_id = $edm->id
            where el.id is null and cc.timecompleted > 0 and cc.course = $edm->course_id";

        $module_completions = $DB->get_records_sql($sql);

        foreach ($module_completions as $mc) {
            // consider days after value
            $seconds_after = $edm->days_after*24*60*60;
            if(($mc->timecompleted + $seconds_after) < time()){  //ie has the time elapsed?
                $user = $DB->get_record('user', array('id'=>$mc->userid));
                send_edm_to_user($edm,$user);
            }
        }
    }
    //Motivational eDM: module specific: sent 4 weeks after completion of module:  
        // purpose is to inspire reps to implement by giving tips and exemplars to help them implement their action plans
    $edms = $DB->get_records('ap_edms',array('name'=>'Motivational EDM','enabled'=>1));
    foreach($edms as $edm){
        $sql = "select cc.* from mdl_course_completions cc
            left join mdl_ap_edm_log el on cc.userid = el.user_id and cc.course = el.course_id and el.edm_id = $edm->id
            where el.id is null and cc.timecompleted > 0  and cc.course = $edm->course_id";

        $module_completions = $DB->get_records_sql($sql);

        foreach ($module_completions as $mc) {
            // consider days after value
            $seconds_after = $edm->days_after*24*60*60; //needs to be after this plus time completed
            if(($mc->timecompleted + $seconds_after) > time()){
                $user = $DB->get_record('user', array('id'=>$mc->userid));
                send_edm_to_user($edm,$user);
            }
        }
    }
    //any targets coming up for users who have not completed?
    //Action plan status nag eDM: send to both club reps if action status is not ‘Complete’ and it is a week before the target 
    //date: contains reminder about the action and its target date and a  link to the Action Plan
    $edms = $DB->get_records('ap_edms',array('name'=>'Target date approaching EDM','enabled'=>1));
    foreach($edms as $edm){
        $sql = "select * from mdl_ap_club_strategies
            where timeframe < DATE_ADD(now(),INTERVAL 7 DAY) and status != 'complete'";

        $clubs_with_targets_approaching = $DB->get_records_sql($sql);

        foreach ($clubs_with_targets_approaching as $ta) {
            // get users belonging to club
            $users = $DB->get_records('ap_club_members', array('club_id'=>$ta->club_id));
            foreach($users as $user){
                if(!edm_has_been_sent($edm,$user)){
                    send_edm_to_user($edm,$user);
                }
            }
        }
    }
   
}
function edm_has_been_sent($user,$edm) {
    global $DB;
    $sql = "SELECT * FROM mdl_ap_edm_log WHERE user_id = $user->id and edm_id = $edm->id";
    return $DB->get_record_sql($sql);
}
function send_edm_to_user($edm,$user) {
    if($edm->enabled == "1"){
        $body_text = get_edm_body_text($edm,$user);
        $from = get_admin();
        $config = get_config('block_action_plans');
        if(isset($config->edm_from_email)){
            $from->email = $config->edm_from_email;
        }
        $subject = $edm->subject;
        $user->mailformat = 1; //force html - this is what the wysiwyg produces
        $result = email_to_user($user, $from, $subject, $body_text, $body_text);
        log_edm($edm,$user,$result);
    }
}
function get_edm_body_text($edm,$user) {
    global $DB;
    
    $edm_body = $edm->body;
    $edm_body = str_ireplace('[user]', $user->firstname . ' ' . $user->lastname, $edm_body);
    $edm_body = str_ireplace('[username]', $user->username, $edm_body);
    $edm_body = str_ireplace('[email]', $user->email, $edm_body);
    // get their club
    $sql = "select c.* from mdl_ap_clubs c
        inner join mdl_ap_club_members cm on c.id = cm.club_id
        inner join mdl_user u on cm.user_id = u.id
        where user_id = $user->id";
    $club = $DB->get_record_sql($sql);
    if($club){
        $edm_body = str_ireplace('[clubname]', $club->name, $edm_body);
    
        //get their club reps
        $sql = "select u.id, u.username from mdl_ap_clubs c
            inner join mdl_ap_club_members cm on c.id = cm.club_id
            inner join mdl_user u on cm.user_id = u.id
            where c.id = $club->id
            order by u.id desc ";
        $club_reps = $DB->get_records_sql($sql);
        $counter = 1;
        foreach ($club_reps as $club_rep) {
            if($counter == 1){
                $edm_body = str_ireplace('[uname_rep_1]', $club_rep->username, $edm_body);
            }
            if($counter == 2){
                $edm_body = str_ireplace('[uname_rep_2]', $club_rep->username, $edm_body);
            }
            $counter++;
        }
    }    
    if(stristr($edm_body,'[module]')){
        $course = $DB->get_record('course',array('id'=>$edm->course_id));
        $edm_body = str_ireplace('[module]', $course->fullname, $edm_body);
    }
    if(stristr($edm_body,'[admin]')){
        $admin = get_admin();
        $edm_body = str_ireplace('[admin]', $admin->firstname . ' ' . $admin->lastname, $edm_body);
    }
    return $edm_body;
}
function log_edm($edm,$user,$result) {
    global $DB,$USER;
    $data = new stdClass();
    $data->edm_id = $edm->id;
    $data->user_id = $user->id;
    $data->course_id = $edm->course_id;
    $data->created_by = $USER->id;
    $data->date_created = date('Y-m-d H:i:s');
    $data->time_sent = date('Y-m-d H:i:s');
    $data->result = $result;
    $newid = $DB->insert_record('ap_edm_log',$data);

}

function enrol_completed_users_in_next_module() {
    global $DB;
    $courseids_to_check = array(15,16,17,18,19);
    $roleid = 5;
    $enrol_manual = enrol_get_plugin('manual');
    //mtrace('Inside enrol completed users in next module. \n');
    foreach ($courseids_to_check as $completion_course_id){
        switch ($completion_course_id) {
            case 15:
                $enrol_course_id = 18;
                break;
            case 16:
                $enrol_course_id = 17;
                break;
            case 17:
                $enrol_course_id = 19;
                break;
            case 18:
                $enrol_course_id = 16;
                break;
            default:
                break;
        }
        $course = $DB->get_record('course',array('id'=>$enrol_course_id));
        if($course){
            $instance = $DB->get_record('enrol', array('courseid' => $enrol_course_id, 'enrol' => 'manual'), '*', MUST_EXIST);
        }
        else{
            continue;
        }
        $sql = "select userid from mdl_scorm_scoes_track t
        inner join mdl_scorm_scoes ss on t.scoid = ss.id
        inner join mdl_scorm s on t.scormid = s.id
        where value = 'completed' 
        and  s.course = $completion_course_id
        and userid not in (
                select ue.userid from mdl_user_enrolments ue
                inner join mdl_enrol e on ue.enrolid = e.id
                where e.courseid = $enrol_course_id
        )
        and userid not in (select id from mdl_user where deleted = 1)
        ";
        $users_to_enrol = $DB->get_records_sql($sql);
        foreach($users_to_enrol as $user_to_enrol){
            $enrol_manual->enrol_user($instance, $user_to_enrol->userid, $roleid);
        }
    }
    //mtrace('End of enrol completed users in next module. \n');
}