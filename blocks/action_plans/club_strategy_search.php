<?php

/**
 * Action Plans Block: Search 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     11/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search club_strategies
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 * */
//params
$sort = optional_param('sort', 'status', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 100, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);
$tab = optional_param('tab', 'club_strategy_search', PARAM_FILE);
$ap_strategies_id = optional_param('ap_strategies_id', 0, PARAM_INT);
$ap_clubs_id = optional_param('ap_clubs_id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);
$filter_status = optional_param('filter_status', '0', PARAM_TEXT); //has to be text because we might get a zero (for no selection) or a text return
$show_available_only = optional_param('show_available_only', 0, PARAM_INT);
$chosen_id = optional_param('chosen_id', 0, PARAM_INT);
$chosen = optional_param('chosen', 0, PARAM_INT);
$strategy_id = optional_param('strategy_id', 0, PARAM_INT);

// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 'tab', 'ap_strategies_id', 'ap_clubs_id', 'user_id', 'filter_status'));
// prepare columns for results table
$columns = array(
    "goal",
    "objective",
    "strategy",
    "timeframe",
    "user_responsible",
    "resources_required",
    "status",
);
if($download == ''){
    $columns[] = "file";
}

foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_action_plans');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    }
    else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&dir=$columndir&sort=$column'>$string[$column]</a>";
    }
    else {
        $$column = $string[$column];
    }
}

$sql = "SELECT c.* FROM mdl_ap_clubs c INNER JOIN mdl_ap_club_members cm on c.id = cm.club_id WHERE user_id = $USER->id";
$user_club = $DB->get_record_sql($sql);

if(!$user_club && $ap_clubs_id > 0){
    // then present the strats for whichever club they picked
    $sql = "SELECT * FROM mdl_ap_clubs WHERE id = $ap_clubs_id";
    $user_club = $DB->get_record_sql($sql);
}

//did they choose one?
if($chosen_id > 0 && $chosen == 1){
    $chosen_one = new stdClass();
    $chosen_one->id = $chosen_id;
    $chosen_one->chosen = 1;
    $chosen_one->modified_by = $USER->id;
    $chosen_one->date_modified = date('Y-m-d h:i:s');
    $DB->update_record('ap_club_strategies',$chosen_one);
}
elseif($chosen==1){
    //means they chose one that doesn't have an id yet, so let's create one
    $chosen_one = new stdClass();
    $chosen_one->strategy_id = $strategy_id;
    $chosen_one->club_id = $user_club->id;
    $chosen_one->created_by = $USER->id;
    $chosen_one->date_created = date('Y-m-d h:i:s');
    $chosen_one->chosen = 1;
    $chosen_one->status = 'notyetstarted';
    $DB->insert_record('ap_club_strategies',$chosen_one);
    //TODO: then redirect to the edit page
}
//figure out the and clause from what has been submitted
$and = '';
if ($search != '') {
    $and .= " and concat(
a.resources_required,
a.status
) like '%$search%'";
}
//are we filtering on ap_strategies?
if ($ap_strategies_id > 0) {
    $and .= " and a.id = $ap_strategies_id ";
}
//are we filtering on ap_clubs?
if ($ap_clubs_id > 0 and !is_siteadmin() and $chosen == 0) {
    $and .= " and mdl_ap_clubs.id = $ap_clubs_id ";
}
//are we filtering on user?
if ($user_id > 0) {
    $and .= " and mdl_user.id = $user_id ";
}
//are we filtering on status?
if ($filter_status != '0') {
    $and .= " and mdl_ap_club_strategies.status = '$filter_status' ";
}

if($show_available_only == 1){
    $and .= " and timecompleted > 0 ";
}
if($download != ''){
   $and .= " and mdl_ap_club_strategies.chosen = 1 ";
}
$club_id_clause = "";
if(isset($user_club->id) && !is_siteadmin()){
    $club_id_clause = "and mdl_ap_club_strategies.club_id = $user_club->id";
}
$q = "select a.id, a.name as strategy,  mdl_ap_clubs.name as club, mdl_files.contextid, mdl_files.component, mdl_files.filearea, 
    mdl_files.filename, mdl_files.itemid, mdl_files.id as fileid, CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user_responsible,
    strategy_id, club_id,  file_id,  timeframe, user_responsible_id,resources_required, mdl_ap_club_strategies.status, 
    o.numeral as objective_numeral, o.name as objective, g.numeral as goal_numeral, g.name as goal, cc.timecompleted, 
    mdl_ap_club_strategies.id as club_strategy_id, chosen, o.course_id
from mdl_ap_strategies a 
LEFT JOIN mdl_ap_club_strategies on a.id = mdl_ap_club_strategies.strategy_id $club_id_clause
LEFT JOIN mdl_ap_objectives o on a.objective_id = o.id
LEFT JOIN mdl_ap_goals g on o.goal_id = g.id
LEFT JOIN mdl_ap_clubs on mdl_ap_club_strategies.club_id = mdl_ap_clubs.id 
LEFT JOIN mdl_files on mdl_ap_club_strategies.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.' 
LEFT JOIN mdl_user on mdl_ap_club_strategies.user_responsible_id = mdl_user.id
LEFT JOIN (select s.course, max(t.timemodified) as timecompleted from mdl_scorm_scoes_track t
    inner join mdl_scorm_scoes ss on t.scoid = ss.id
    inner join mdl_scorm s on t.scormid = s.id
    where value = 'completed' and userid = $USER->id
    group by s.course
    ) cc on o.course_id = cc.course
where 1 = 1 
$and 
order by g.numeral, o.numeral, a.id ASC";
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '';
}
if ($download == '') {
//get a page worth of records
    $results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
}
else {
    $results = $DB->get_records_sql($q);
}
if ($download == '') {
//also get the total number we have of these
    $q = "SELECT COUNT(DISTINCT a.id)
from mdl_ap_strategies a 
LEFT JOIN mdl_ap_club_strategies on a.id = mdl_ap_club_strategies.strategy_id  $club_id_clause 
LEFT JOIN mdl_ap_objectives o on a.objective_id = o.id
LEFT JOIN mdl_ap_goals g on o.goal_id = g.id
LEFT JOIN mdl_ap_clubs on mdl_ap_club_strategies.club_id = mdl_ap_clubs.id 
LEFT JOIN mdl_files on mdl_ap_club_strategies.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.' 
LEFT JOIN mdl_user on mdl_ap_club_strategies.user_responsible_id = mdl_user.id 
LEFT JOIN (
    select s.course, max(t.timemodified) as timecompleted 
    from mdl_scorm_scoes_track t 
    inner join mdl_scorm_scoes ss on t.scoid = ss.id 
    inner join mdl_scorm s on t.scormid = s.id 
    where value = 'completed' and userid = $USER->id 
    group by s.course
) cc on o.course_id = cc.course
where 1 = 1  $and";

    if (isset($_GET['debug'])) {
        echo '$query : ' . $q . '<br>';
    }

    echo "<style>.greyed_out{background-color:e6e6e6;}</style>";
    $result_count = $DB->get_field_sql($q);
    if(isset($user_club->id)){
        echo $OUTPUT->heading('Action plan for club: ' . $user_club->name);
    }
    else{
        echo $OUTPUT->heading("Please choose a club");
    }
    require_once('club_strategy_search_form.php');
    $mform = new club_strategy_search_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'search' => $search, 'tab' => $currenttab, 'ap_strategies_id' => $ap_strategies_id, 'ap_clubs_id' => $ap_clubs_id, 'user_id' => $user_id, 'context' => $context, 'user_club'=>$user_club));
    $mform->display();
    echo '<table width="100%" style="margin-bottom:20px"><tr><td>';
    echo get_string('instructions_step1', 'block_action_plans');
    echo '</td></tr><tr><td >';
    echo get_string('instructions_step2', 'block_action_plans');
    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if ($download == '') {
        echo $OUTPUT->heading(get_string('noresults', 'block_action_plans', $search));
    }
}
else {
    $table = new html_table();
    $table->head = array(
        $objective,
        $strategy,
        $timeframe,
        $user_responsible,
        $resources_required,
        $status
    );
    $table->wrap = array(null, null, 'nowrap',null, null, null);
    if($download == ''){
        $table->head[] = $file;
        $table->head[] = 'Action';
    }
    $table->width = "95%";
    $row_counter = 0;
    foreach ($results as $result) {
        $choose_link = ''; //enable if wanted: "<a href='index.php?tab=club_strategy_view&id=$result->id'>View</a> "; 
        $edit_link = "";
        $delete_link = "";
        
        if ($result->timecompleted > 0 || is_siteadmin() || $result->course_id == 20){
            if ($result->chosen == 0 & isset($user_club->id)) {
                $choose_url = "index.php?tab=club_strategy_search&strategy_id=$result->id&chosen=1&ap_clubs_id=$user_club->id";
                
                if(isset($result->club_strategy_id)){
                    $choose_url .= "&chosen_id=$result->club_strategy_id";
                }
                $choose_link = "<a href='$choose_url'>Choose</a> ";
            }
            else if(isset($user_club->id)){
                // then we show the edit link
                $edit_link = "<a href='index.php?tab=club_strategy_edit&strategy_id=$result->id&club_id=$user_club->id&id=$result->club_strategy_id'>Update</a> ";
            }
        }
//        if (has_capability('block/action_plans:delete', $context)) {
//// then we show the delete link
//            $delete_link = "<a href='index.php?tab=club_strategy_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
//        }
        //link for file
        $file_link = '';
        if (isset($result->filename)) {
            $file_link = new moodle_url("/pluginfile.php/{$result->contextid}/{$result->component}/{$result->filearea}/" . $result->itemid . '/' . $result->filename);
        }
        $timeframe_date = "";
        if ($result->timeframe != null) {
            $timeframe_date = date('d-m-Y', strtotime($result->timeframe));
        }
        $status_string = "";
        if ($result->status != '') {
            $status_string = get_string($result->status, 'block_action_plans');
        }
        $data_array = array(
            $result->objective_numeral . '. ' . $result->objective,
            $result->strategy,
            $timeframe_date,
            $result->user_responsible,
            $result->resources_required,
            $status_string
        );
        
        if($download == ''){
            $data_array[] = html_writer::link($file_link, $result->filename);
            $data_array[] = $choose_link . $edit_link . $delete_link;
        }
        $table->data[] = $data_array;
        // figure out whether to grey this row out or not
        if ($result->timecompleted > 0) {
            $table->rowclasses[$row_counter] = 'normal';
        }
        else {
            $table->rowclasses[$row_counter] = 'greyed_out';
        }
        $row_counter++;
    }
}
if (!empty($table)) {
    if ($download != '') {
        //export the table to whatever they asked for
        block_action_plans_export_data($download, $table, $tab,'action_plan_report',null,$user_club->name);
    } else {
        echo html_writer::table($table);
        $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);
        block_action_plans_output_download_links($PAGE->url, 'action_plan_heading');
    }
}

