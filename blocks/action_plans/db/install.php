<?php
/** 
 * Action Plans Block: Install DB scripts 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/


function xmldb_block_action_plans_install() {
global $DB;
$dbman = $DB->get_manager();
$result = true;
// create tables for the block
if (!$dbman->table_exists('ap_action_plans')) {
$table = new xmldb_table('ap_action_plans');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '50', null, XMLDB_NOTNULL, null, null);
$table->add_field('club_name', XMLDB_TYPE_CHAR , '50', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('ap_edms')) {
$table = new xmldb_table('ap_edms');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '50', null, XMLDB_NOTNULL, null, null);
$table->add_field('body', XMLDB_TYPE_TEXT , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('enabled', XMLDB_TYPE_INTEGER , '4', null, XMLDB_NOTNULL, null, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('trigger_point_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('days_after', XMLDB_TYPE_INTEGER , '11', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$table->add_index('ix_trigger_point_id', XMLDB_INDEX_NOTUNIQUE, array('trigger_point_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('ap_goals')) {
$table = new xmldb_table('ap_goals');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '100', null, XMLDB_NOTNULL, null, null);
$table->add_field('action_plan_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_action_plan_id', XMLDB_INDEX_NOTUNIQUE, array('action_plan_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('ap_objectives')) {
$table = new xmldb_table('ap_objectives');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '255', null, XMLDB_NOTNULL, null, null);
$table->add_field('goal_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_goal_id', XMLDB_INDEX_NOTUNIQUE, array('goal_id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('ap_strategies')) {
$table = new xmldb_table('ap_strategies');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('strategy', XMLDB_TYPE_TEXT , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('objective_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('timeframe', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('user_responsible_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('resources_required', XMLDB_TYPE_TEXT , 'null', null, XMLDB_NULL, null, null);
$table->add_field('status', XMLDB_TYPE_CHAR , '15', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_objective_id', XMLDB_INDEX_NOTUNIQUE, array('objective_id'));
$table->add_index('ix_user_responsible_id', XMLDB_INDEX_NOTUNIQUE, array('user_responsible_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('ap_trigger_points')) {
$table = new xmldb_table('ap_trigger_points');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '50', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$dbman->create_table($table);
}
return $result;
} 