<?php

/** 
 * Action Plans Block: Search 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     18/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search objectives
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 **/

//params
$sort   = optional_param('sort', 'numeral', PARAM_RAW);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 20, PARAM_INT); 
$search = optional_param('search', '', PARAM_TEXT);
$tab    = optional_param('tab', 'objective_search', PARAM_FILE);
$ap_goals_id = optional_param('ap_goals_id', 0, PARAM_INT); 
$course_id = optional_param('course_id', 0, PARAM_INT); 
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort','dir','page','perpage','search','tab','ap_goals_id','course_id'));
// prepare columns for results table
$columns = array(
"name",
"numeral",
"goal",
"course",
);
foreach ($columns as $column) {
$string[$column] = get_string("$column",'block_action_plans');
if ($sort != $column) {
$columnicon = '';
$columndir = 'ASC';
} else {
$columndir = $dir == 'ASC' ? 'DESC':'ASC';
$columnicon = $dir == 'ASC' ? 'down':'up';
}
if($column != 'details'){
$$column = "<a href='$PAGE->url&dir=$columndir&sort=$column'>$string[$column]</a>";
}
else{
 $$column = $string[$column];
}
}
//figure out the and clause from what has been submitted
$and = '';
if($search != ''){
$and .= " and a.name like '%$search%'";
} 
//are we filtering on ap_goals?
if($ap_goals_id > 0){ 
$and .= " and mdl_ap_goals.id = $ap_goals_id ";
} 
//are we filtering on course?
if($course_id > 0){ 
$and .= " and mdl_course.id = $course_id ";
} 
$q = "select DISTINCT a.* , mdl_ap_goals.name as goal, mdl_course.fullname as course 
from mdl_ap_objectives a 
LEFT JOIN mdl_ap_goals  on a.goal_id = mdl_ap_goals.id
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where 1 = 1 
$and 
order by $sort $dir";
if(isset($_GET['debug'])){echo '$query : ' . $q . ''   ;}
if($download == ''){
//get a page worth of records
$results = $DB->get_records_sql($q,array(),$page*$perpage, $perpage);
}
else{
$results = $DB->get_records_sql($q);
}
if($download == ''){
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
from mdl_ap_objectives a 
LEFT JOIN mdl_ap_goals  on a.goal_id = mdl_ap_goals.id
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
 where 1 = 1  $and";

if(isset($_GET['debug'])){echo '$query : ' . $q . '<br>'   ;}

$result_count = $DB->get_field_sql($q);

require_once('objective_search_form.php');
$mform = new objective_search_form(null, array('sort' => $sort,'dir'=>$dir,'perpage'=>$perpage,'search'=>$search,'tab'=>$currenttab ,'ap_goals_id'=>$ap_goals_id,'course_id'=>$course_id));
$mform->display();
echo '<table width="100%"><tr><td width="50%">';
echo $result_count . ' ' . get_string('objective_plural','block_action_plans') . " found" . '<br>';
echo '</td><td style="text-align:right;">';
if(has_capability('block/action_plans:edit', $context)){
echo "<a href='index.php?tab=objective_new'>" . get_string('objective_new','block_action_plans') . "</a>";
}
echo '</td></tr></table>';

}

//RESULTS
if (!$results) {
if($download == ''){
echo $OUTPUT->heading(get_string('noresults','block_action_plans',$search));
}
} else {
$table = new html_table();
$table->head = array (
$name,
$numeral,
$goal,
$course,
'Action'
);
$table->width = "95%";
foreach ($results as $result) {
$view_link = '';//enable if wanted: "<a href='index.php?tab=objective_view&id=$result->id'>View</a> "; 
$edit_link = "";
$delete_link = "";
if(has_capability('block/action_plans:edit', $context)){
// then we show the edit link
$edit_link = "<a href='index.php?tab=objective_edit&id=$result->id'>Edit</a> ";
}
if(has_capability('block/action_plans:delete', $context)){
// then we show the delete link
$delete_link = "<a href='index.php?tab=objective_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
}
$table->data[] = array (
$result->name,
$result->numeral,
$result->goal,
$result->course,
$view_link . $edit_link . $delete_link
);
}
}
if(!empty($table)){
if ($download != '') {
//export the table to whatever they asked for
block_action_plans_export_data($download,$table,$tab);
}
else {
echo html_writer::table($table);
$pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
$pagingbar->pagevar = 'page';
echo $OUTPUT->render($pagingbar);
}
}

