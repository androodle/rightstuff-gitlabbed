<?php

/** 
 * Action Plans Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     02/06/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class club_action_plan_edit_form extends moodleform {
protected $club_action_plan;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.* , mdl_ap_action_plans.name as action_plan, mdl_ap_clubs.name as club 
from mdl_ap_club_action_plans a 
LEFT JOIN mdl_ap_action_plans  on a.action_plan_id = mdl_ap_action_plans.id
LEFT JOIN mdl_ap_clubs  on a.club_id = mdl_ap_clubs.id
where a.id = {$_REQUEST['id']} ";
$club_action_plan = $DB->get_record_sql($q);
}
else{
$club_action_plan = $this->_customdata['$club_action_plan']; // this contains the data of this form
}
$tab = 'club_action_plan_new'; // from whence we were called
if (!empty($club_action_plan->id)) {
$tab = 'club_action_plan_edit';
}
$mform->addElement('html','<div>');

//action_plan_id
$options = $DB->get_records_menu('ap_action_plans',null,'name','id,name'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'action_plan_id', get_string('action_plan','block_action_plans'), $options);
$mform->addRule('action_plan_id', get_string('required'), 'required', null, 'server');

//club_id
$options = $DB->get_records_menu('ap_clubs',null,'name','id,name'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'club_id', get_string('club','block_action_plans'), $options);
$mform->addRule('club_id', get_string('required'), 'required', null, 'server');

//target_date
$mform->addElement('date_selector', 'target_date', get_string('target_date','block_action_plans'));
$mform->addRule('target_date', get_string('required'), 'required', null, 'server');
//set values if we are in edit mode
if (!empty($club_action_plan->id) && isset($_GET['id'])) {
$mform->setConstant('action_plan_id', $club_action_plan->action_plan_id);
$mform->setConstant('club_id', $club_action_plan->club_id);
$mform->setConstant('target_date', strtotime($club_action_plan->target_date));
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->setType('tab', PARAM_TEXT);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
$mform->setType('id', PARAM_INT);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
$mform->setType('id', PARAM_INT);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
