<?php

/** 
 * Action Plans Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     12/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class action_plan_edit_form extends moodleform {
protected $action_plan;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.*  
from mdl_ap_action_plans a 
where a.id = {$_REQUEST['id']} ";
$action_plan = $DB->get_record_sql($q);
}
else{
$action_plan = $this->_customdata['$action_plan']; // this contains the data of this form
}
$tab = 'action_plan_new'; // from whence we were called
if (!empty($action_plan->id)) {
$tab = 'action_plan_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_action_plans'), array('size'=>50));
$mform->setType('name', PARAM_RAW);
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 50 characters', 'maxlength', 50, 'client');
//set values if we are in edit mode
if (!empty($action_plan->id) && isset($_GET['id'])) {
$mform->setConstant('name', $action_plan->name);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->setType('tab', PARAM_TEXT);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
$mform->setType('id', PARAM_INT);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
$mform->setType('id', PARAM_INT);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
