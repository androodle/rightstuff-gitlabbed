<?php

/** 
 * Action Plans Block: Search 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     21/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search trigger_points
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 **/

//params
$sort   = optional_param('sort', 'name', PARAM_RAW);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 20, PARAM_INT); 
$search = optional_param('search', '', PARAM_TEXT);
$tab    = optional_param('tab', 'trigger_point_search', PARAM_FILE);
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page','perpage','search','tab'));

if(!is_siteadmin()){
    echo "Access denied";
    exit();
}
// prepare columns for results table
$columns = array(
"name",
);
foreach ($columns as $column) {
$string[$column] = get_string("$column",'block_action_plans');
if ($sort != $column) {
$columnicon = '';
$columndir = 'ASC';
} else {
$columndir = $dir == 'ASC' ? 'DESC':'ASC';
$columnicon = $dir == 'ASC' ? 'down':'up';
}
if($column != 'details'){
$$column = "<a href='$PAGE->url&dir=$columndir&sort=$column'>$string[$column]</a>";
}
else{
 $$column = $string[$column];
}
}
//figure out the and clause from what has been submitted
$and = '';
if($search != ''){
$and .= " and a.name like '%$search%'";
} 
$q = "select DISTINCT a.*  
from mdl_ap_trigger_points a 
where 1 = 1 
$and 
order by $sort $dir";
if(isset($_GET['debug'])){echo '$query : ' . $q . ''   ;}
//get a page worth of records
$results = $DB->get_records_sql($q,array(),$page*$perpage, $perpage);
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
from mdl_ap_trigger_points a 
 where 1 = 1  $and";

if(isset($_GET['debug'])){echo '$query : ' . $q . '<br>'   ;}

$result_count = $DB->get_field_sql($q);
if($download == ''){

require_once('trigger_point_search_form.php');
$mform = new trigger_point_search_form(null, array('sort' => $sort,'dir'=>$dir,'perpage'=>$perpage,'search'=>$search,'tab'=>$currenttab ));
$mform->display();
echo '<table width="100%"><tr><td width="50%">';
echo $result_count . ' ' . get_string('trigger_point_plural','block_action_plans') . " found" . '<br>';
echo '</td><td style="text-align:right;">';
if(has_capability('block/action_plans:edit', $context)){
echo "<a href='index.php?tab=trigger_point_new'>" . get_string('trigger_point_new','block_action_plans') . "</a>";
}
echo '</td></tr></table>';

}

//RESULTS
if (!$results) {
if($download == ''){
echo $OUTPUT->heading(get_string('noresults','block_action_plans',$search));
}
} else {
$table = new html_table();
$table->head = array (
$name,
'Action'
);
$table->width = "95%";
foreach ($results as $result) {
$view_link = '';//enable if wanted: "<a href='index.php?tab=trigger_point_view&id=$result->id'>View</a> "; 
$edit_link = "";
$delete_link = "";
if(has_capability('block/action_plans:edit', $context)){
// then we show the edit link
$edit_link = "<a href='index.php?tab=trigger_point_edit&id=$result->id'>Edit</a> ";
}
if(has_capability('block/action_plans:delete', $context)){
// then we show the delete link
$delete_link = "<a href='index.php?tab=trigger_point_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
}
$table->data[] = array (
$result->name,
$view_link . $edit_link . $delete_link
);
}
}
if(!empty($table)){
if ($download != '') {
//export the table to whatever they asked for
block_action_plans_export_data($download,$table,$tab);
}
else {
echo html_writer::table($table);
$pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
$pagingbar->pagevar = 'page';
echo $OUTPUT->render($pagingbar);
//echo 'Download: ';    
//$download_report_url = $PAGE->url . "&download=csv";
//$download_report_url = str_ireplace("&amp;", "&", $download_report_url);
//echo html_writer::link($download_report_url, 'CSV');
//echo "&nbsp;";
//$download_report_url = $PAGE->url . "&download=excel";
//$download_report_url = str_ireplace("&amp;", "&", $download_report_url);
//echo html_writer::link($download_report_url, 'Excel');
}
}

