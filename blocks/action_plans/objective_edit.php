<?php

/** 
 * Action Plans Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     18/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the objectives
 *  
 **/

global $OUTPUT;
require_capability('block/action_plans:edit', $context);
require_once('objective_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , mdl_ap_goals.name as goal, mdl_course.fullname as course 
from mdl_ap_objectives a 
LEFT JOIN mdl_ap_goals  on a.goal_id = mdl_ap_goals.id
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where a.id = $id ";
$objective = $DB->get_record_sql($q);
$mform = new objective_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$DB->update_record('ap_objectives',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_action_plans'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('objective_edit', 'block_action_plans'));
$mform->display();
}

