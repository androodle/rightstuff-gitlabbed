<?php

    defined('MOODLE_INTERNAL') || die;
    
    global $DB;
    
    $temp = new admin_settingpage('localbackup_escreencontrol', new lang_string('pluginname', 'local_escreencontrol'));
    
    $escreens = $DB->get_records_menu('escreen',null,'name',"id,name");
    $temp->add(new admin_setting_configselect('local_escreencontrol/signup_escreen',
            new lang_string('signup_escreen', 'local_escreencontrol'),
            new lang_string('signup_escreen_help', 'local_escreencontrol'), 2,
            $escreens));

    if($ADMIN->locate('localplugins')) {
        $ADMIN->add('localplugins', $temp);
    }