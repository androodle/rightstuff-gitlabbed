<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function local_escreencontrol_enrol_user(&$user) {
	
	global $DB;
        
        $escreen = local_escreencontrol_get_signup_escreen();
	
	$courseid = $escreen->course; 
	$escreenid = $escreen->cmid; 
	$roleid = 5; // (student) TODO into admin settings
	
	$enrol_manual = enrol_get_plugin('manual');
	if (!$enrol_manual) { 
		throw new coding_exception('Can not instantiate enrol_manual'); 
	}
	
	// The user has to be confirmed in order to proceed.
	$user->confirmed = 1;
	$DB->set_field("user", "confirmed", 1, array("id" => $user->id));
	
	$instance = $DB->get_record('enrol', array('courseid' => $courseid, 'enrol' => 'manual'), '*', MUST_EXIST);
	$enrolled = $enrol_manual->enrol_user($instance, $user->id, $roleid);
	if(!isloggedin()){
            // Authenticate user if they are not logged in already
            authenticate_user_login($user->username, $user->password_plain);
            unset($user->password_plain);
            $user->deleted = 0;
            $user->currentlogin = time();
            $user->lastlogin = time();
            $user->policyagreed = 1;
            complete_user_login($user);
        }
	
	
}

// This function should return true whether the escreen is completed
// or in process of completion (so as not to redirect user
// to the beginning of it again)
function local_escreencontrol_escreen_completed() {
	global $USER, $SESSION, $CFG, $DB;
	if (!empty($SESSION->escreen)) {
		return true;
	}
	$escreen = local_escreencontrol_get_signup_escreen();
	if (!$escreen) {
            return true;
	}
        
        //require_once($CFG->dirroot.'/mod/escreen/lib.php');
	
	$params = array('userid'=>$USER->id, 'escreen'=>$escreen->id);
	return $DB->record_exists('escreen_tracking', $params);
}

function local_escreencontrol_redirect_user($escreenid) {
	redirect(new moodle_url('/mod/escreen/view.php?id=' . $escreenid));
}

function local_escreencontrol_get_signup_escreen(){
    global $DB;
    $config = get_config('local_escreencontrol');
    if(isset($config->signup_escreen)){
        $sql = "select e.*, cm.id as cmid from mdl_escreen e
            inner join mdl_course_modules cm on e.id = cm.instance
            inner join mdl_modules m on cm.module = m.id
            where m.name = 'escreen' and e.id = " . $config->signup_escreen;
        return $DB->get_record_sql($sql);
    }
    return false;
}