<?php

/**
 * @author Felipe Carasso
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package moodle multiauth
 *
 * Authentication Plugin: Email Authentication with admin confirmation
 *
 * Standard authentication function.
 *
 * 2012-12-03  File created based on 'email' package by Martin Dougiamas.
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir.'/authlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');


/**
 * Email authentication plugin.
 */
class auth_plugin_emailadmin extends auth_plugin_base {

    /**
     * Constructor.
     */
    function auth_plugin_emailadmin() {
        $this->authtype = 'emailadmin';
        $this->config = get_config('auth/emailadmin');
    }

    /**
     * Returns true if the username and password work and false if they are
     * wrong or don't exist.
     *
     * @param string $username The username
     * @param string $password The password
     * @return bool Authentication success or failure.
     */
    function user_login ($username, $password) {
        global $CFG, $DB;
        if ($user = $DB->get_record('user', array('username'=>$username, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            return validate_internal_user_password($user, $password);
        }
        return false;
    }

    /**
     * Updates the user's password.
     *
     * called when the user password is updated.
     *
     * @param  object  $user        User table object  (with system magic quotes)
     * @param  string  $newpassword Plaintext password (with system magic quotes)
     * @return boolean result
     *
     */
    function user_update_password($user, $newpassword) {
        $user = get_complete_user_data('id', $user->id);
        return update_internal_user_password($user, $newpassword);
    }

    function can_signup() {
        return true;
    }

    /**
     * Sign up a new user ready for confirmation.
     * Password is passed in plaintext.
     *
     * @param object $user new user object
     * @param boolean $notify print notice with link and terminate
     */
    function user_signup($user, $notify=true) {
        global $CFG, $DB;
        require_once($CFG->dirroot.'/user/profile/lib.php');
        //BEGIN CLIENT-SPECIFIC CODE: RETAIN
        //separate the supplementary user data out
        $supplementary_user = new stdClass;
        foreach ($user as $key => $value){
            if(stristr($key,'supplementary_')){
                $plain_key = str_ireplace('supplementary_','', $key);
                $supplementary_user->$plain_key = $value;
                unset($user->$key);
            }
            //and copy the profile field data to the sup user too
            if(stristr($key,'profile_field_')){
                $supplementary_user->$key = $value;
            }
        }
        //END CLIENT-SPECIFIC CODE: RETAIN
        
        //deal with primary user
        $matchcount = $this->is_duplicate($user);
        if ($matchcount) {
            global $CFG, $PAGE, $OUTPUT;
            $duplicate = get_string('duplicate', 'auth_emailadmin');
            $PAGE->navbar->add($duplicate);
            $PAGE->set_title($duplicate);
            $PAGE->set_heading($PAGE->course->fullname);
            echo $OUTPUT->header();
            notice(get_string('duplicate_message', 'auth_emailadmin'), "$CFG->wwwroot/index.php");
            return false;
        }
	// CLIENT-SPECIFIC CHANGE: RETAIN
	$user->password_plain = $user->password; //need this for escreen auth, for primary user
        $user->password = hash_internal_user_password($user->password);
        if (empty($user->calendartype)) {
            $user->calendartype = $CFG->calendartype;
        }
        $user->id = $DB->insert_record('user', $user);

        // Check that the custom profile field exists, create if id doesn't
        $field = $DB->get_record('user_info_field', array('shortname' => 'accounttype'));
        if (!$field) {
            $sql = "SELECT * FROM {user_info_category} ORDER BY id ASC LIMIT 1";
            $category = $DB->get_record_sql($sql);
            if (!$category) {
                $category = new stdclass();
                $category->name = 'Other fields';
                $category->sortorder = 1;
                $category = $DB->insert_record('user_info_category', $category);
                $sortorder = 1;
            } else {
                $sql = "SELECT * FROM {user_info_field} WHERE categoryid = '{$category->id}' ORDER BY sortorder DESC LIMIT 1";
                $lastfield = $DB->get_record_sql($sql);
                $sortorder = $lastfield->sortorder;
            }
            $field = new stdclass();
            $field->shortname = 'accounttype';
            $field->name = get_string('selfregistereduser', 'auth_emailadmin');
            $field->datatype = 'checkbox';
            $field->descriptionformat = '1';
            $field->categoryid = $category->id;
            $field->sortorder = $sortorder;
            $field->required = '0';
            $field->locked = '1';
            $field->visible = '0';
            $field->forceunique = '0';
            $field->signup = '0';
            $field->defaultdata = '0';
            $field->defaultdataformat = '0';
            $DB->insert_record('user_info_field', $field);
        }
        $user->profile_field_accounttype = 1;

        /// Save any custom profile field information
        profile_save_data($user);

        // BEGIN CLIENT-SPECIFIC CHANGE: RETAIN
        //save the club in separate relational table
        require_once($CFG->dirroot."/blocks/action_plans/lib.php");
        $club_id = ensure_club_exists($user->profile_field_clubname);
        ensure_user_is_club_member($club_id, $user->id);
        
        //send rego edm
        $edm = $DB->get_record('ap_edms',array('name'=>'Registration EDM')); // if they ever change the name this will break, but how else?
        send_edm_to_user($edm,$user);
        
        //save supplementary user if there is one
        if(!empty($supplementary_user->username) && !empty($supplementary_user->password) && !empty($supplementary_user->email)
                && !empty($supplementary_user->firstname) && !empty($supplementary_user->lastname)) {
            $matchcount = $this->is_duplicate($supplementary_user);
            if ($matchcount) {
                global $CFG, $PAGE, $OUTPUT;
                $duplicate = get_string('duplicate', 'auth_emailadmin');
                $PAGE->navbar->add($duplicate);
                $PAGE->set_title($duplicate);
                $PAGE->set_heading($PAGE->course->fullname);
                echo $OUTPUT->header();
                notice(get_string('duplicate_message', 'auth_emailadmin'), "$CFG->wwwroot/index.php");
                return false;
            }
            $supplementary_user->password = hash_internal_user_password($supplementary_user->password);
            if (empty($supplementary_user->calendartype)) {
                $supplementary_user->calendartype = $CFG->calendartype;
            }
            $supplementary_user->confirmed   = 0;
            $supplementary_user->lang        = current_language();
            $supplementary_user->firstaccess = time();
            $supplementary_user->timecreated = time();
            $supplementary_user->mnethostid  = $CFG->mnet_localhost_id;
            $supplementary_user->secret      = random_string(15);
            $supplementary_user->auth        = $CFG->registerauth;
            
            $supplementary_user->id = $DB->insert_record('user', $supplementary_user);
            
            ensure_user_is_club_member($club_id, $supplementary_user->id);
            
            //now save the profile info of the primary user to the secondary one
            $supplementary_user->profile_field_accounttype = 1;
            profile_save_data($supplementary_user);
        } 
        
	if (file_exists($CFG->dirroot.'/local/escreencontrol/lib.php')) {
		require_once($CFG->dirroot.'/local/escreencontrol/lib.php');
		local_escreencontrol_enrol_user($user);
	}
        //enrol users in modules 1 and 6
        $roleid = 5;
        $course_ids = array(15,20);
        $enrol_manual = enrol_get_plugin('manual');
        foreach($course_ids as $courseid){
            $instance = $DB->get_record('enrol', array('courseid' => $courseid, 'enrol' => 'manual'), '*', MUST_EXIST);
            $enrol_manual->enrol_user($instance, $user->id, $roleid);
            if(isset($supplementary_user->id)){
                $enrol_manual->enrol_user($instance, $supplementary_user->id, $roleid);
            }
        }
	// END CLIENT-SPECIFIC CHANGE: RETAIN

        $user = $DB->get_record('user', array('id'=>$user->id));
        //events_trigger('user_created', $user);
        $usercontext = context_user::instance($user->id);
        $event = \core\event\user_created::create(
            array(
                'objectid' => $user->id,
                'relateduserid' => $user->id,
                'context' => $usercontext
                )
            );
        $event->trigger();
// we are marking them as confirmed so there is no need for this
//        if (! $this->send_confirmation_email_support($user)) {
//            print_error('auth_emailadminnoemail','auth_emailadmin');
//        }

        if ($notify) {
            global $CFG;
            //BEGIN CLIENT-SPECIFIC CODE: RETAIN
            //send them to the survey
            $escreen = local_escreencontrol_get_signup_escreen();
            $survey_url = $CFG->wwwroot."/mod/escreen/complete.php?gopage=0&id=".$escreen->cmid;
            redirect($survey_url);
            
            //END CLIENT-SPECIFIC CODE: RETAIN
        } else {
            return true;
        }
    }

    /**
     * Returns true if plugin allows confirming of new users.
     *
     * @return bool
     */
    function can_confirm() {
        return true;
    }

    /**
     * Confirm the new user as registered.
     *
     * @param string $username
     * @param string $confirmsecret
     */
    function user_confirm($username, $confirmsecret) {
        global $DB;
        $user = get_complete_user_data('username', $username);

        if (!empty($user)) {
            if ($user->confirmed) {
                return AUTH_CONFIRM_ALREADY;

            } else if ($user->auth != $this->authtype) {
                mtrace("Auth mismatch for user ". $user->username .": ". $user->auth ." != ". $this->authtype);
                return AUTH_CONFIRM_ERROR;

            } else if ($user->secret == $confirmsecret) {   // They have provided the secret key to get in
                $DB->set_field("user", "confirmed", 1, array("id"=>$user->id));
                if ($user->firstaccess == 0) {
                    $DB->set_field("user", "firstaccess", time(), array("id"=>$user->id));
                }
                return AUTH_CONFIRM_OK;
            }
        } else {
            mtrace("User not found: ". $username);
            return AUTH_CONFIRM_ERROR;
        }
    }

    function prevent_local_passwords() {
        return false;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return true;
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return true;
    }

    /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url() {
        return null; // use default internal method
    }

    /**
     * Returns true if plugin allows resetting of internal password.
     *
     * @return bool
     */
    function can_reset_password() {
        return true;
    }

    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields) {
        include "config.html";
    }

    /**
     * Processes and stores configuration data for this authentication plugin.
     */
    function process_config($config) {
        // set to defaults if undefined
        if (!isset($config->recaptcha)) {
            $config->recaptcha = false;
        }

        // save settings
        set_config('recaptcha', $config->recaptcha, 'auth/emailadmin');
        set_config('emailoption', $config->emailoption, 'auth/emailadmin');
//        set_config('allowsignupposition', $config->allowsignupposition, 'totara_hierarchy');
//        set_config('allowsignuporganisation', $config->allowsignuporganisation, 'totara_hierarchy');
//        set_config('allowsignupmanager', $config->allowsignupmanager, 'totara_hierarchy');
        return true;
    }

    /**
     * Returns whether or not the captcha element is enabled, and the admin settings fulfil its requirements.
     * @return bool
     */
    function is_captcha_enabled() {
        global $CFG;
        return isset($CFG->recaptchapublickey) && isset($CFG->recaptchaprivatekey) && get_config("auth/{$this->authtype}", 'recaptcha');
    }

    /**
     * Send email to admin with confirmation text and activation link for
     * new user.
     *
     * @param user $user A {@link $USER} object
     * @return bool Returns true if mail was sent OK to *any* admin and false if otherwise.
     */
    function send_confirmation_email_support($user) {
        global $CFG;
    
        $site = get_site();
        $supportuser = core_user::get_support_user();
    

        $data = new stdClass();
        $data->firstname = fullname($user);
        $data->sitename  = format_string($site->fullname);
        $data->admin     = generate_email_signoff();

        $data->userdata = '';
        foreach(((array) $user) as $dataname => $datavalue) {
            $data->userdata	 .= $dataname . ': ' . $datavalue . PHP_EOL;
        }

        // Add custom fields
        $data->userdata .= $this->list_custom_fields($user);
    
        $subject = get_string('auth_emailadminconfirmationsubject', 'auth_emailadmin', format_string($site->fullname));
    
        $username = urlencode($user->username);
        $username = str_replace('.', '%2E', $username); // prevent problems with trailing dots
        $data->link  = $CFG->wwwroot .'/auth/emailadmin/confirm.php?data='. $user->secret .'/'. $username;
        $message     = get_string('auth_emailadminconfirmation', 'auth_emailadmin', $data);
        $messagehtml = text_to_html(get_string('auth_emailadminconfirmation', 'auth_emailadmin', $data), false, false, true);
    
        $user->mailformat = 1;  // Always send HTML version as well
    
        // Get users to send the email to.
        if (!isset($this->config->emailoption)) {
            $this->config->emailoption = 'alladmins';
        }
        switch ($this->config->emailoption) {
            case 'firstadmin':
                $admins = get_admin();
                break;
            case 'usecapability':
                $context = context_system::instance();
                $admins = get_users_by_capability($context, 'auth/emailadmin:receiveemails');
                if (!empty($admins)) {
                    break;
                    // ...otherwise continue and fallback to 'alladmins'
                }
            case 'alladmins':
                $admins = get_admins();
            default:
                break;
        }

        //directly email rather than using the messaging system to ensure its not routed to a popup or jabber
        $return = true;

        foreach ($admins as $admin) {
            if (!email_to_user($admin, $supportuser, $subject, $message, $messagehtml)) {
                $return = false;
            }
        }

        return $return;
    }

    /**
     * Return an array with custom user properties.
     *
     * @param user $user A {@link $USER} object
     */
    function list_custom_fields($user) {
        global $CFG, $DB;

        $result = '';
        if ($fields = $DB->get_records('user_info_field')) {
            foreach($fields as $field) {
                $fieldobj = new profile_field_base($field->id, $user->id);
                $result .= format_string($fieldobj->field->name.':') . ' ' . $fieldobj->display_data() . PHP_EOL;
            }
        }

        return $result;
    }

    function is_duplicate($user) {
        global $CFG;

        $incfile = $CFG->dirroot.'/blocks/salesforce_integration/usermatch.class.php';
        if (!file_exists($incfile)) {
            return 0;
        }
        
        require_once($incfile);

        $checkuser = clone($user);
        if (!empty($checkuser->profile_field_dateofbirth)) {
            $date = userdate($checkuser->profile_field_dateofbirth, '%Y-%m-%d');
            $checkuser->profile_field_dateofbirth = $date;
        }

/*
        Fields that can be passed are:
        firstname
        lastname
        email
        profile_field_portalid
        profile_field_hrid
        profile_field_salesforceid
        profile_field_dateofbirth (in the format YYYY-MM-DD)
 */

        $UserMatch = new UserMatch();
        $matchcount = $UserMatch->findMatch($checkuser);

        if ($matchcount == 1 and !$UserMatch->isReviewRequired()) {
            $userid = $UserMatch->getMatchID(1);
            $username = $UserMatch->getMatchUsername(1);
        }

        return $matchcount;
    }
}
